# Shop App


**The application is a pet project. Which use PrivatBank API.**

[![Build Status](https://gitlab.com/VolodiaSachenko/shop-client/badges/master/pipeline.svg )](https://gitlab.com/VolodiaSachenko/shop-client/-/commits/master)

## Technologies ##


* Java 8
* Spring MVC
* Spring JPA
* Spring Security
* Hibernate
* JPA annotations
* PostgreSQL
* Lombok
* JSTL
* SLF4J
* Maven
* Rest template
* Swagger
* JUnit
* Mockito
* Docker