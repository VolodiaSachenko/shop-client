<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Cart List</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <sec:authorize access="hasRole('ADMIN')">
            <div class="container">
                <button type="button" class="btn btn-primary" onclick="location.href = '/cart/add'">Add</button>
            </div>
        </sec:authorize>
        <div>
            <h2 style="position: relative; left: 45%">Carts</h2>
            <table class="table table-striped">
                <tr>
                    <th>Name</th>
                    <th>Owner email</th>
                    <th>Shop</th>
                    <th>Sum UAH</th>
                    <th>Sum USD</th>
                    <th>Sum EUR</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach items="${carts}" var="cart">
                    <tr>
                        <td>${cart.name}</td>
                        <td>${cart.person.email}</td>
                        <td>${cart.shop.name}</td>
                        <td>${cart.sum}</td>
                        <td>${cart.usd}</td>
                        <td>${cart.euro}</td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/cart/id${cart.id}/products">
                                <button type="submit" class="btn btn-outline-info">
                                    Products
                                </button>
                            </form>
                        </td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/cart/update/id${cart.id}">
                                <button type="submit" class="btn btn-outline-success">
                                    Edit
                                </button>
                            </form>
                        </td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/cart/delete/id${cart.id}">
                                <button type="submit" class="btn btn-danger">
                                    Remove
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>