<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Product List</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <div class="container">
            <button type="button" class="btn btn-primary" onclick="location.href = '/product/add'">Add</button>
        </div>
        <div>
            <h2 style="position: relative; left: 45%">Products</h2>
            <table class="table table-striped">
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Shop</th>
                    <th>Price UAH</th>
                    <th>Price USD</th>
                    <th>Price EUR</th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach items="${products}" var="product">
                    <tr>
                        <td>${product.name}</td>
                        <td>${product.description}</td>
                        <td>${product.shopName}</td>
                        <td>${product.price}</td>
                        <td>${product.usd}</td>
                        <td>${product.euro}</td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/product/update/id${product.id}">
                                <button type="submit" class="btn btn-outline-success">
                                    Edit
                                </button>
                            </form>
                        </td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/product/delete/id${product.id}">
                                <button type="submit" class="btn btn-danger">
                                    Remove
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>