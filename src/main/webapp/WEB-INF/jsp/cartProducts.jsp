<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Cart Products</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <div>
            <h2 style="position: relative; left: 45%">Cart products</h2>
            <table class="table table-striped">
                <tr>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th>Price</th>
                    <th></th>

                </tr>
                <c:forEach items="${cartDto.products}" var="cartProduct">
                    <tr>
                        <td>${cartProduct.name}</td>
                        <td>${cartProduct.description}</td>
                        <td>${cartProduct.price}</td>
                        <td>
                            <form method="post"
                                  action="${pageContext.request.contextPath}/cart/delete/id${cartProduct.id}/${cartDto.id}">
                                <button type="submit" class="btn btn-danger">
                                    Remove
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <br>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>