<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add new product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 style="position: relative; left: 35%">Add new product</h1>
    <form:form action="add/create" method="post" modelAttribute="product"
               cssStyle="width: 500px;position: relative;left: 30%;">
        <div class="mb-3">
            <form:label path="name" cssClass="form-label">Name:</form:label>
            <form:input path="name" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="description" cssClass="form-label">Description:</form:label>
            <form:input path="description" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="shopName" cssClass="form-label">Shop:</form:label>
            <form:select cssClass="form-control" path="shopName" items="${shops}"/>
        </div>

        <div class="mb-3">
            <form:label path="price" cssClass="form-label">Price:</form:label>
            <br>
            <fmt:formatNumber type="number"
                              pattern="0.00" value="${product.price}"
                              var="sum"/>
            <label>
                <input type="number" class="form-control" min="0" name="price" pattern="0.00"
                       step=".01" required
                       value="${sum}"/>
            </label>
        </div>
        <form:button class="btn btn-primary">Add</form:button><br><br>
        <div style="text-align: left">
            <a href="http://localhost:8080/">Return to home page</a>
        </div>
    </form:form>
</div>
</body>
</html>
