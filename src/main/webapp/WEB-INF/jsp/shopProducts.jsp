<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Product List</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <div>
            <h2 style="position: relative; left: 45%">Shop products</h2>
            <table class="table table-striped">
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Shop</th>
                    <th>Price UAH</th>
                    <th>Price USD</th>
                    <th>Price EUR</th>
                    <th></th>
                </tr>
                <c:forEach items="${shopProducts}" var="shopProduct">
                    <tr>
                        <td>${shopProduct.name}</td>
                        <td>${shopProduct.description}</td>
                        <td>${shopProduct.shopName}</td>
                        <td>${shopProduct.price}</td>
                        <td>${shopProduct.usd}</td>
                        <td>${shopProduct.euro}</td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/shop/id${market.id}/add-to-cart/product-id${shopProduct.id}">
                                <button type="submit" class="btn btn-outline-success">
                                    Add to cart
                                </button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>