<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Shops</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <sec:authorize access="hasRole('ADMIN')">
            <div class="container">
                <button type="button" class="btn btn-primary" onclick="location.href = '/shop/add'">Add</button>
            </div>
        </sec:authorize>
        <div>
            <h2 style="position: relative; left: 45%">Shops</h2>
            <table class="table table-striped">
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Phone</th>
                    <th>Rating</th>
                    <th></th>
                    <sec:authorize access="hasRole('ADMIN')">
                        <th></th>
                        <th></th>
                    </sec:authorize>
                </tr>
                <c:forEach items="${shops}" var="shop">
                    <tr>
                        <td>${shop.name}</td>
                        <td>${shop.description}</td>
                        <td>${shop.phone}</td>
                        <td>${shop.rating}</td>
                        <td>
                            <form method="get"
                                  action="${pageContext.request.contextPath}/shop/id${shop.id}/products/">
                                <button type="submit" class="btn btn-outline-info">
                                    Products
                                </button>
                            </form>
                        </td>
                        <sec:authorize access="hasRole('ADMIN')">
                            <td>
                                <form method="get"
                                      action="${pageContext.request.contextPath}/shop/update/id${shop.id}">
                                    <button type="submit" class="btn btn-outline-success">
                                        Edit
                                    </button>
                                </form>
                            </td>
                            <td>
                                <form method="get"
                                      action="${pageContext.request.contextPath}/shop/delete/id${shop.id}">
                                    <button type="submit" class="btn btn-danger">
                                        Remove
                                    </button>
                                </form>
                            </td>
                        </sec:authorize>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>