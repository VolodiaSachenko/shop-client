<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Person Info</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
          xmlns="http://www.w3.org/1999/html">
</head>
<body>
<div class="container">
    <div>
        <h1 style="position: relative; left: 35%"></h1>
        <div>
            <sec:authorize access="hasRole('ADMIN')">
                <h2 style="position: relative; left: 45%">Customers</h2>
            </sec:authorize>
            <sec:authorize access="hasRole('CUSTOMER')">
                <h2 style="position: relative; left: 45%">User information</h2>
            </sec:authorize>
            <table class="table table-striped">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <sec:authorize access="hasRole('ADMIN')">
                        <th></th>
                    </sec:authorize>
                    <sec:authorize access="hasRole('CUSTOMER')">
                        <th>Login</th>
                    </sec:authorize>
                    <th></th>
                </tr>
                <c:forEach items="${persons}" var="person">
                    <tr>
                        <td>${person.firstName}</td>
                        <td>${person.surName}</td>
                        <td>${person.email}</td>
                        <td>${person.phone}</td>
                        <sec:authorize access="hasRole('CUSTOMER')">
                            <td>${person.username}</td>
                        </sec:authorize>
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/person/update/id${person.id}">
                                <button type="submit" class="btn btn-outline-success">
                                    Edit
                                </button>
                            </form>
                        </td>
                        <sec:authorize access="hasRole('ADMIN')">
                            <td>
                                <form method="get"
                                      action="${pageContext.request.contextPath}/person/delete/id${person.id}">
                                    <button type="submit" class="btn btn-danger">
                                        Remove
                                    </button>
                                </form>
                            </td>
                        </sec:authorize>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
    <a href="http://localhost:8080/">Return to home page</a>
</div>
</body>
</html>