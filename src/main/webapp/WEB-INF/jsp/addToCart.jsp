<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add to cart</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 style="position: relative; left: 35%">Add product to cart</h1>
    <form:form action="/confirm-order" method="post" modelAttribute="order"
               cssStyle="width: 500px;position: relative;left: 30%;">
        <div class="mb-3">
            <form:hidden path="shop.id" cssClass="form-control"/>
            <form:hidden path="cartProduct.id" cssClass="form-control"/>
        </div>
        <div class="mb-3">
            <form:label path="cartProduct.name" cssClass="form-label">Name:</form:label>
            <form:hidden path="cartProduct.name" cssClass="form-control"/>
                ${order.cartProduct.name}
        </div>
        <div class="mb-3">
            <form:label path="cartProduct.description" cssClass="form-label">Description:</form:label>
            <form:hidden path="cartProduct.description" cssClass="form-control"/>
                ${order.cartProduct.description}
        </div>
        <div class="mb-3">
            <form:label path="shop.name" cssClass="form-label">Shop name:</form:label>
            <form:hidden path="shop.name" cssClass="form-control"/>
                ${order.shop.name}
        </div>
        <div class="mb-3">
            <form:label path="cartProduct.price" cssClass="form-label">Price:</form:label>
            <form:hidden path="cartProduct.price" cssClass="form-control"/>
                ${order.cartProduct.price}
        </div>
        <sec:authorize access="hasRole('ADMIN')">
            <div class="mb-3">
                <form:label path="person.email" cssClass="form-label">Client email:</form:label>
                <form:select path="person.email" cssClass="form-control" items="${customers}"/>
            </div>
        </sec:authorize>
        <sec:authorize access="hasRole('CUSTOMER')">
            <div class="mb-3">
                <form:label path="person.username" cssClass="form-label">Login:</form:label>
                <form:hidden path="person.username" cssClass="form-control"/>
                    ${principal.name}
            </div>
        </sec:authorize>
        <form:button class="btn btn-primary">Add</form:button><br><br>
        <div style="text-align: left">
            <a href="http://localhost:8080/">Return to home page</a>
        </div>
    </form:form>
</div>
</body>
</html>
