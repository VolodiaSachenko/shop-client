<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>Add new cart</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 style="position: relative; left: 35%">Add new cart</h1>
    <form:form action="add/create" method="post" modelAttribute="cart"
               cssStyle="width: 500px;position: relative;left: 30%;">
        <div class="mb-3">
            <form:label path="name" cssClass="form-label">Name:</form:label>
            <form:input path="name" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="shop.name" cssClass="form-label">Shop:</form:label>
            <form:select path="shop.name" cssClass="form-control" items="${availableShops}"/>
        </div>
        <div class="mb-3">
            <form:hidden path="sum" cssClass="form-control"/>
        </div>
        <div class="mb-3">
            <form:label path="person.email" cssClass="form-label">Owner email:</form:label>
            <form:select path="person.email" cssClass="form-control" items="${cartOwners}"/>
        </div>
        <form:button class="btn btn-primary">Add</form:button><br><br>
        <div style="text-align: left">
            <a href="http://localhost:8080/">Return to home page</a>
        </div>
    </form:form>
</div>
</body>
</html>
