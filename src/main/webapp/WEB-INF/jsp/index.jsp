<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page errorPage="error.jsp" %>
<!DOCTYPE HTML>
<html>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
      xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <title>Shopping App</title>
</head>
<body>
<div class="container" style="text-align: center;">
    <sec:authorize access="isAuthenticated()">
        <div style="alignment: left">
            <h4>Welcome,<a href="${pageContext.request.contextPath}/person/edit"> ${principal}</a></h4>
        </div>
    </sec:authorize>
    <h1>Shopping App</h1>
    <h1 style="position: relative; text-align: center "></h1><br>
</div>
<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div style="text-align: center">
                        <sec:authorize access="hasRole('ADMIN')">
                            <a href="${pageContext.request.contextPath}/person">Persons</a><br>
                        </sec:authorize>
                        <a href="${pageContext.request.contextPath}/cart">Carts</a><br>
                        <sec:authorize access="hasRole('ADMIN')">
                            <a href="${pageContext.request.contextPath}/product">Products</a><br>
                        </sec:authorize>
                        <a href="${pageContext.request.contextPath}/shop">Shops</a><br>
                        <a href="${pageContext.request.contextPath}/logout">Log out</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>



