<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<!doctype html>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"
      xmlns="http://www.w3.org/1999/html">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <title>Shop app</title>
</head>
<body>
<div class="container" style="text-align: center;">
    <h1>Shop app</h1></div>
<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Please login
                    </div>
                    <div class="card-body">
                        <form:form action="login/confirm" method="post" modelAttribute="login"
                                   cssStyle="width: 500px;position: relative;left: 30%;">
                            <div class="mb-3">
                                <form:label path="username" cssClass="form-label">Login:</form:label>
                                <form:input path="username" cssClass="form-control" required="required"/>
                            </div>
                            <div class="mb-3">
                                <form:label path="password" cssClass="form-label">Password:</form:label>
                                <form:password path="password" cssClass="form-control" required="required"/>
                            </div>
                            <form:button class="btn btn-primary">Log In</form:button><br><br>
                            <div style="text-align: left">
                                <a href="${pageContext.request.contextPath}/registration">Sign Up</a>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</body>
</html>

