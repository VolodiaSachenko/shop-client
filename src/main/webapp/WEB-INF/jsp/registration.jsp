<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page errorPage="error.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1 style="position: relative; left: 41%">Registration</h1>
    <form:form action="/registration/confirm" method="post" modelAttribute="registrationForm"
               cssStyle="width: 500px;position: relative;left: 30%;">
        <div class="mb-3">
            <form:label path="firstName" cssClass="form-label">Name:</form:label>
            <form:input path="firstName" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="surName" cssClass="form-label">Surname:</form:label>
            <form:input path="surName" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="username" cssClass="form-label">Username:</form:label>
            <form:input path="username" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="phone" cssClass="form-label">Phone:</form:label>
            <form:input path="phone" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="email" cssClass="form-label">Email:</form:label>
            <form:input type="email" path="email" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="password" cssClass="form-label">Password:</form:label>
            <form:password path="password" cssClass="form-control" required="required"/>
        </div>
        <div class="mb-3">
            <form:label path="passwordConfirm" cssClass="form-label">Confirm password:</form:label>
            <form:password path="passwordConfirm" cssClass="form-control" required="required"/>
        </div>
        <form:button class="btn btn-primary">Add</form:button><br><br>
        <div style="text-align: left">
            <a href="http://localhost:8080/">Return to home page</a>
        </div>
    </form:form>
</div>
</body>
</html>
