create schema if not exists market;

create table if not exists market.persons
(
    id           bigint not null
        primary key,
    email        varchar(255),
    first_name   varchar(255),
    password     varchar(255),
    phone        varchar(255),
    registration timestamp,
    last_name    varchar(255),
    username     varchar(255)
);

create table if not exists market.roles
(
    id   bigint not null
        primary key,
    name varchar(255)
);

create table if not exists market.persons_roles
(
    persons_id bigint not null
        constraint fk3w6njofhcuxvneuj3x1scwggs
            references market.persons,
    roles_id   bigint not null
        constraint fkc2410odf5r9gxmjbalnk99nu3
            references market.roles,
    primary key (persons_id, roles_id)
);

create table if not exists market.shops
(
    id          bigint not null
        primary key,
    description varchar(255),
    name        varchar(255),
    phone       varchar(255),
    rating      real   not null
);

create table if not exists market.products
(
    id          bigint not null
        primary key,
    date        timestamp,
    description varchar(255),
    name        varchar(255),
    price       numeric(19, 2),
    shop_id     bigint
        constraint fk7kp8sbhxboponhx3lxqtmkcoj
            references market.shops
);

create table if not exists market.carts
(
    id        bigint not null
        primary key,
    name      varchar(255),
    date      timestamp,
    sum       numeric(19, 2),
    person_id bigint
        constraint fkn7i4384wn1xk85949e3a6fjwv
            references market.persons,
    shop_id   bigint
        constraint fkuw3798o19ixwd5do7dt385sw
            references market.shops
);

create table if not exists market.carts_products
(
    cart_id     bigint not null
        constraint fk3nvguygrfbn661omvvu2uafu5
            references market.carts,
    products_id bigint not null
        constraint fkq9ns7lphr8im6vg3i5dwknsbn
            references market.products
);