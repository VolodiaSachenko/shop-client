package com.ithillel.shopclient.repository;

import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    Optional<List<Product>> findByShopId(Shop shop);

}
