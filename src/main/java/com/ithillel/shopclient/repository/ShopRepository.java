package com.ithillel.shopclient.repository;

import com.ithillel.shopclient.model.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShopRepository extends CrudRepository<Shop, Long> {

    Optional<Shop> findByName(String name);

}
