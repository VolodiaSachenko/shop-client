package com.ithillel.shopclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
@Accessors(chain = true)
public class CurrencyExchange {
    private String ccy;
    @JsonProperty("base_ccy")
    private String baseCcy;
    private BigDecimal buy;
    private BigDecimal sale;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyExchange that = (CurrencyExchange) o;
        return ccy.equals(that.ccy) && baseCcy.equals(that.baseCcy) &&
                buy.equals(that.buy) && sale.equals(that.sale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ccy, baseCcy, buy, sale);
    }

    @Override
    public String toString() {
        return "CurrencyExchange{" +
                "ccy='" + ccy + '\'' +
                ", baseCcy='" + baseCcy + '\'' +
                ", buy=" + buy +
                ", sale=" + sale +
                '}';
    }
}
