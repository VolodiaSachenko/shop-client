package com.ithillel.shopclient.rest.client;

public interface PrivateRestClient {

    Object[] getObjectsFromJson();

}
