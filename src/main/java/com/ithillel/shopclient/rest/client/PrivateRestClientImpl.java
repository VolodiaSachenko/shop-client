package com.ithillel.shopclient.rest.client;

import com.ithillel.shopclient.model.CurrencyExchange;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PrivateRestClientImpl implements PrivateRestClient {

    private static final String URL_GET_JSON = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";

    @Override
    public CurrencyExchange[] getObjectsFromJson() {
        RestTemplate restTemplate = new RestTemplate();
        CurrencyExchange[] currencyArray = restTemplate.getForObject(URL_GET_JSON, CurrencyExchange[].class);

        if (currencyArray != null) {
            return currencyArray;
        } else {
            return new CurrencyExchange[0];
        }
    }
}
