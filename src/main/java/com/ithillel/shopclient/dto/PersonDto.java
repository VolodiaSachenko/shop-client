package com.ithillel.shopclient.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public final class PersonDto {
    private Long id;
    private String firstName;
    private String surName;
    private String phone;
    private String email;
    private String username;
    private String password;
    private String passwordConfirm;

    @Override
    public String toString() {
        return firstName + " " + surName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDto personDto = (PersonDto) o;
        return id.equals(personDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
