package com.ithillel.shopclient.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
public final class ProductDto {
    private Long id;
    private String name;
    private String description;
    private String shopName;
    private BigDecimal price;
    private BigDecimal euro;
    private BigDecimal usd;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDto that = (ProductDto) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", shopName='" + shopName + '\'' +
                ", price=" + price +
                ", euro=" + euro +
                ", usd=" + usd +
                '}';
    }
}
