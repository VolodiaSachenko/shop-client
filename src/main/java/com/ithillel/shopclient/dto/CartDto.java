package com.ithillel.shopclient.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public final class CartDto {
    private Long id;
    private PersonDto person;
    private ShopDto shop;
    private String name;
    private BigDecimal sum;
    private BigDecimal euro;
    private BigDecimal usd;
    private ProductDto cartProduct;
    private List<ProductDto> products;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CartDto cartDto = (CartDto) o;
        return id.equals(cartDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "CartDto{" +
                "id=" + id +
                ", person=" + person +
                ", shop=" + shop +
                ", name='" + name + '\'' +
                ", sum=" + sum +
                ", euro=" + euro +
                ", usd=" + usd +
                ", cartProduct=" + cartProduct +
                ", products=" + products +
                '}';
    }
}
