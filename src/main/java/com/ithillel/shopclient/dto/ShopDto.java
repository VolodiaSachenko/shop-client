package com.ithillel.shopclient.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@Getter
@Setter
public final class ShopDto {
    private Long id;
    private String name;
    private String description;
    private String phone;
    private float rating;
    private List<ProductDto> products;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShopDto shopDto = (ShopDto) o;
        return id.equals(shopDto.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
