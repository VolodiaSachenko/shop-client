package com.ithillel.shopclient.utils.mapper;

import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.model.Shop;

import java.util.ArrayList;
import java.util.stream.Collectors;

public final class ShopMapper {

    public static Shop fromDto(ShopDto shopDto) {
        Shop shop = new Shop();
        shop.setName(shopDto.getName());
        shop.setDescription(shopDto.getDescription());
        shop.setPhone(shopDto.getPhone());
        shop.setId(shopDto.getId());
        shop.setRating(shopDto.getRating());
        shop.setProducts(new ArrayList<>());
        if (shopDto.getProducts() != null) {
            shop.setProducts(shopDto.getProducts()
                    .stream()
                    .map(ProductMapper::fromDto)
                    .collect(Collectors.toList()));
        }
        return shop;
    }

    public static ShopDto toDto(Shop shop) {
        ShopDto shopDto = new ShopDto();
        shopDto.setId(shop.getId());
        shopDto.setName(shop.getName());
        shopDto.setDescription(shop.getDescription());
        shopDto.setPhone(shop.getPhone());
        shopDto.setRating(shop.getRating());
        shopDto.setProducts(new ArrayList<>());
        if (shop.getProducts() != null) {
            shopDto.setProducts(shop.getProducts()
                    .stream()
                    .map(ProductMapper::toDto)
                    .collect(Collectors.toList()));
        }
        return shopDto;
    }
}
