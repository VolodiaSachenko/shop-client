package com.ithillel.shopclient.utils.mapper;

import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;

import java.util.Date;

public final class ProductMapper {

    public static Product fromDto(ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setDateCreate(new Date());
        product.setPrice(productDto.getPrice());
        Shop shop = new Shop();
        shop.setName(productDto.getShopName());
        product.setShopId(shop);
        return product;
    }

    public static ProductDto toDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setDescription(product.getDescription());
        if (product.getShopId() != null) {
            productDto.setShopName(product.getShopId().getName());
        }
        productDto.setPrice(product.getPrice());

        return productDto;
    }
}
