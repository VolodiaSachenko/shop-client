package com.ithillel.shopclient.utils.mapper;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.model.Cart;

import java.util.ArrayList;
import java.util.stream.Collectors;

public final class CartMapper {

    public static Cart fromDto(CartDto cartDto) {
        Cart cart = new Cart();
        cart.setName(cartDto.getName());
        cart.setId(cartDto.getId());
        cart.setSum(cartDto.getSum());
        if (cartDto.getPerson() != null) {
            cart.setPersonId(PersonMapper.fromDto(cartDto.getPerson()));
        }
        if (cartDto.getShop() != null) {
            cart.setShopId(ShopMapper.fromDto(cartDto.getShop()));
        }
        if (cartDto.getProducts() != null) {
            cart.setProducts(cartDto.getProducts()
                    .stream()
                    .map(ProductMapper::fromDto)
                    .collect(Collectors.toList()));
        }
        return cart;
    }

    public static CartDto toDto(Cart cart) {
        CartDto cartDto = new CartDto();
        cartDto.setId(cart.getId());
        cartDto.setName(cart.getName());
        if (cart.getPersonId() != null) {
            cartDto.setPerson(PersonMapper.toDto(cart.getPersonId()));
        }
        if (cart.getShopId() != null) {
            cartDto.setShop(ShopMapper.toDto(cart.getShopId()));
        }
        cartDto.setSum(cart.getSum());
        cartDto.setProducts(new ArrayList<>());
        if (cart.getProducts() != null) {
            cartDto.setProducts(cart.getProducts()
                    .stream()
                    .map(ProductMapper::toDto)
                    .collect(Collectors.toList()));
        }
        return cartDto;
    }
}
