package com.ithillel.shopclient.utils.mapper;

import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.model.Person;

import java.util.ArrayList;
import java.util.Date;

public final class PersonMapper {

    public static Person fromDto(PersonDto personDto) {
        return new Person()
                .setId(personDto.getId())
                .setFirstName(personDto.getFirstName())
                .setSurName(personDto.getSurName())
                .setPhone(personDto.getPhone())
                .setRegistrationDate(new Date())
                .setCarts(new ArrayList<>())
                .setEmail(personDto.getEmail())
                .setUsername(personDto.getUsername())
                .setPasswordConfirm(personDto.getPasswordConfirm())
                .setPassword(personDto.getPassword());
    }

    public static PersonDto toDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.setId(person.getId());
        personDto.setFirstName(person.getFirstName());
        personDto.setSurName(person.getSurName());
        personDto.setEmail(person.getEmail());
        personDto.setPhone(person.getPhone());
        personDto.setUsername(person.getUsername());

        return personDto;
    }
}
