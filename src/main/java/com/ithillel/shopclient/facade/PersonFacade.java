package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.PersonDto;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

public interface PersonFacade {

    PersonDto save(PersonDto personDto) throws MessagingException;

    PersonDto findPersonById(String id);

    PersonDto findByUsername(String login);

    List<PersonDto> getAllPersons(Principal principal, HttpServletRequest request);

    void updatePerson(PersonDto personDto);

    void deletePersonById(String id);

}
