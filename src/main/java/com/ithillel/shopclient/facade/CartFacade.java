package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.CartDto;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

public interface CartFacade {

    CartDto save(CartDto cartDto);

    CartDto findById(String id);

    List<CartDto> getAll(Principal principal, HttpServletRequest request);

    CartDto update(CartDto cartDto);

    void deleteById(String id);

    void addCartForm(Model model);

    void deleteProduct(String productId, String cartId);

}
