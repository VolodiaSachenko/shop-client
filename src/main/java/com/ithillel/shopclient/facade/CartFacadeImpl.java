package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.service.CartService;
import com.ithillel.shopclient.service.PersonService;
import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.ShopService;
import com.ithillel.shopclient.utils.mapper.CartMapper;
import com.ithillel.shopclient.utils.mapper.ShopMapper;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CartFacadeImpl implements CartFacade {
    private final CartService cartService;
    private final PersonService personService;
    private final ShopService shopService;
    private final CurrencyExchangeService currencyExchangeService;

    public CartFacadeImpl(CartService cartService, PersonService personService, ShopService shopService, CurrencyExchangeService currencyExchangeService) {
        this.cartService = cartService;
        this.personService = personService;
        this.shopService = shopService;
        this.currencyExchangeService = currencyExchangeService;
    }

    @Override
    public CartDto save(CartDto cartDto) {
        Cart cart = CartMapper.fromDto(cartDto);
        cartService.save(cart);
        return cartDto;
    }

    @Override
    public CartDto findById(String id) {
        Cart cart = cartService.findById(Long.parseLong(id));
        return CartMapper.toDto(cart);
    }

    @Override
    public List<CartDto> getAll(Principal principal, HttpServletRequest request) {
        List<CartDto> carts;
        if (request.isUserInRole("ADMIN")) {
            carts = cartService.getAll()
                    .stream()
                    .map(this::setCurrency)
                    .collect(Collectors.toList());
        } else {
            carts = cartService.getAll()
                    .stream()
                    .filter(cart ->
                            Objects.equals(cart.getPersonId().getUsername(), principal.getName()))
                    .map(this::setCurrency)
                    .collect(Collectors.toList());
        }
        return carts;
    }

    @Override
    public CartDto update(CartDto cartDto) {
        Cart cart = CartMapper.fromDto(cartDto);
        cartService.update(cart);
        return cartDto;
    }

    @Override
    public void deleteById(String id) {
        cartService.deleteById(Long.parseLong(id));
    }

    @Override
    public void addCartForm(Model model) {
        List<String> persons = personService.getAllPersons()
                .stream()
                .map(Person::getEmail)
                .collect(Collectors.toList());
        List<ShopDto> shops = shopService.getAllShops()
                .stream()
                .map(ShopMapper::toDto)
                .collect(Collectors.toList());
        model.addAttribute("cart", new CartDto());
        model.addAttribute("cartOwners", persons);
        model.addAttribute("availableShops", shops);
    }

    @Override
    public void deleteProduct(String productId, String cartId) {
        Cart cart = cartService.findById(Long.parseLong(cartId));
        shopService.deleteProductFromCart(
                cart.getPersonId().getId(), Long.parseLong(productId), cart.getShopId().getId());
    }

    private CartDto setCurrency(Cart cart) {
        CartDto cartDto = CartMapper.toDto(cart);
        cartDto.setEuro(currencyExchangeService.exchangeToEuros(cart.getSum()));
        cartDto.setUsd(currencyExchangeService.exchangeToDollars(cart.getSum()));

        return cartDto;
    }
}
