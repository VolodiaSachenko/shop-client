package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.ProductService;
import com.ithillel.shopclient.service.ShopService;
import com.ithillel.shopclient.utils.mapper.ProductMapper;
import com.ithillel.shopclient.utils.mapper.ShopMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductFacadeImpl implements ProductFacade {
    private final ProductService productService;
    private final ShopService shopService;
    private final CurrencyExchangeService currencyExchangeService;

    public ProductFacadeImpl(ProductService productService, ShopService shopService,
                             CurrencyExchangeService currencyExchangeService) {
        this.productService = productService;
        this.shopService = shopService;
        this.currencyExchangeService = currencyExchangeService;
    }

    @Override
    public ProductDto save(ProductDto productDto) {
        Product product = ProductMapper.fromDto(productDto);
        productService.save(product);
        return productDto;
    }

    @Override
    public ProductDto findProductById(String id) {
        Product product = productService.findProductById(Long.parseLong(id));
        return ProductMapper.toDto(product);
    }

    @Override
    public List<ProductDto> getAllProducts() {
        return productService.getAllProducts().stream()
                .map(this::setCurrency)
                .collect(Collectors.toList());
    }

    @Override
    public void updateProduct(ProductDto productDto) {
        Product product = ProductMapper.fromDto(productDto);
        productService.updateProduct(product);
    }

    @Override
    public void deleteProductById(String id) {
        productService.deleteProductById(Long.parseLong(id));
    }

    @Override
    public List<ShopDto> addProductForm() {
        return shopService.getAllShops().stream()
                .map(ShopMapper::toDto)
                .collect(Collectors.toList());
    }

    private ProductDto setCurrency(Product product) {
        ProductDto productDto = ProductMapper.toDto(product);
        productDto.setEuro(currencyExchangeService.exchangeToEuros(product.getPrice()));
        productDto.setUsd(currencyExchangeService.exchangeToDollars(product.getPrice()));

        return productDto;
    }
}
