package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.exception.ValidateException;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.PersonService;
import com.ithillel.shopclient.service.ProductService;
import com.ithillel.shopclient.service.ShopService;
import com.ithillel.shopclient.utils.mapper.ProductMapper;
import com.ithillel.shopclient.utils.mapper.ShopMapper;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ShopFacadeImpl implements ShopFacade {
    private final ShopService shopService;
    private final ProductService productService;
    private final PersonService personService;
    private final CurrencyExchangeService currencyExchangeService;

    public ShopFacadeImpl(ShopService shopService, ProductService productService, PersonService personService, CurrencyExchangeService currencyExchangeService) {
        this.shopService = shopService;
        this.productService = productService;
        this.personService = personService;
        this.currencyExchangeService = currencyExchangeService;
    }

    @Override
    public void createShop(ShopDto shopDto) {
        validateName(shopDto.getName());
        Shop shop = ShopMapper.fromDto(shopDto);
        shopService.createShop(shop);
    }

    @Override
    public ShopDto findShopById(String id) {
        Shop shop = shopService.findShopById(Long.parseLong(id));
        return ShopMapper.toDto(shop);
    }

    @Override
    public List<ShopDto> getAllShops() {
        return shopService.getAllShops().stream()
                .map(ShopMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void getAllProducts(String id, Model model) {
        ShopDto shop = ShopMapper.toDto(shopService.findShopById(Long.parseLong(id)));
        List<ProductDto> shopProducts = shopService.getAllProducts(ShopMapper.fromDto(shop))
                .stream()
                .map(this::setCurrency)
                .collect(Collectors.toList());
        model.addAttribute("shopProducts", shopProducts);
        model.addAttribute("market", shop);
    }

    @Override
    public void addToCartForm(String shop, String id, Model model, Principal principal) {
        CartDto order = createCartDto(shop, id, principal);
        List<String> persons = personService.getAllPersons()
                .stream()
                .map(Person::getEmail)
                .collect(Collectors.toList());
        model.addAttribute("order", order);
        model.addAttribute("customers", persons);
        model.addAttribute("principal", principal);
    }

    @Override
    public void updateShop(ShopDto shopDto) {
        Shop shop = ShopMapper.fromDto(shopDto);
        shopService.updateShop(shop);
    }

    @Override
    public void deleteShopById(String id) {
        shopService.deleteShopById(Long.parseLong(id));
    }

    @Override
    public void addProductToCart(CartDto cartDto) {
        Long personId;
        if (cartDto.getPerson().getEmail() != null) {
            personId = personService.findByEmail(cartDto.getPerson().getEmail()).getId();
        } else {
            personId = personService.findByUsername(cartDto.getPerson().getUsername()).getId();
        }
        shopService.addProductToCart(personId, cartDto.getCartProduct().getId(),
                cartDto.getShop().getId());
    }

    private CartDto createCartDto(String shop, String id, Principal principal) {
        CartDto cartDto = new CartDto();
        cartDto.setShop(ShopMapper.toDto(shopService.findShopById(Long.parseLong(shop))));
        cartDto.setCartProduct(ProductMapper.toDto(productService.findProductById(Long.parseLong(id))));
        cartDto.setPerson(new PersonDto());
        cartDto.getPerson().setUsername(principal.getName());

        return cartDto;
    }

    private void validateName(String name) {
        if (shopService.findByName(name).isPresent()) {
            throw new ValidateException("Name " + name + " already exists.");
        }
    }

    private ProductDto setCurrency(Product product) {
        ProductDto productDto = ProductMapper.toDto(product);
        productDto.setEuro(currencyExchangeService.exchangeToEuros(product.getPrice()));
        productDto.setUsd(currencyExchangeService.exchangeToDollars(product.getPrice()));

        return productDto;
    }
}
