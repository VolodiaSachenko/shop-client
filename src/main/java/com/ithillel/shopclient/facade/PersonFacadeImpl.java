package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.exception.ValidateException;
import com.ithillel.shopclient.model.Email;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.service.EmailService;
import com.ithillel.shopclient.service.PersonService;
import com.ithillel.shopclient.utils.mapper.PersonMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class PersonFacadeImpl implements PersonFacade {
    private final PersonService personService;
    private final EmailService emailService;

    public PersonFacadeImpl(PersonService personService, EmailService emailService) {
        this.personService = personService;
        this.emailService = emailService;
    }

    @Transactional
    @Override
    public PersonDto save(PersonDto personDto) throws MessagingException {
        requestCheck(personDto);
        validateEmail(personDto.getEmail());
        validatePhone(personDto.getPhone());
        validateUsername(personDto.getUsername());
        Person person = PersonMapper.fromDto(personDto);
        personService.save(person);
        if (personService.existsByUsername(person.getUsername())) {
            emailService.sendEmail(sendRegistrationEmail(personDto));
        }
        return personDto;
    }

    @Override
    public PersonDto findPersonById(String id) {
        Person person = personService.findPersonById(Long.parseLong(id));
        return PersonMapper.toDto(person);
    }

    @Override
    public PersonDto findByUsername(String login) {
        Person person = personService.findByUsername(login);
        return PersonMapper.toDto(person);
    }

    @Override
    public List<PersonDto> getAllPersons(Principal principal, HttpServletRequest request) {
        List<PersonDto> persons;
        if (request.isUserInRole("ADMIN")) {
            persons = personService.getAllPersons()
                    .stream()
                    .filter(person -> Objects.equals(person.getRoles()
                            .stream()
                            .findFirst()
                            .orElseThrow(() -> new NotFoundException("Role not found"))
                            .getName(), "ROLE_CUSTOMER"))
                    .map(PersonMapper::toDto)
                    .collect(Collectors.toList());
        } else {
            persons = personService.getAllPersons()
                    .stream()
                    .filter(person -> Objects.equals(person.getUsername(), principal.getName()))
                    .map(PersonMapper::toDto)
                    .collect(Collectors.toList());
        }
        return persons;
    }

    @Override
    public void updatePerson(PersonDto personDto) {
        requestCheck(personDto);
        Person person = PersonMapper.fromDto(personDto);
        personService.updatePerson(person);
    }

    @Override
    public void deletePersonById(String id) {
        personService.deletePersonById(Long.parseLong(id));
    }

    private void validateEmail(String email) {
        if (personService.existsByEmail(email) || patternMatches(email, "^(.+)@(\\\\S+)$")) {
            throw new ValidateException("Invalid email " + email);
        }
    }

    private void validatePhone(String phone) {
        if (personService.existsByPhone(phone) || patternMatches(phone,
                "^(\\\\d{3}[- .]?){2}\\\\d{4}$")) {
            throw new ValidateException("Invalid phone number " + phone);
        }
    }

    private void validateUsername(String login) {
        if (personService.existsByUsername(login)) {
            throw new ValidateException("Login " + login + " already exists.");
        }
    }

    private static void requestCheck(PersonDto person) {
        if (person.getFirstName() == null || person.getSurName() == null || person.getEmail() == null) {
            throw new InvalidRequestException("Fields firstName, surName and email are required.");
        }
    }

    private static boolean patternMatches(String value, String regexPattern) {
        return Pattern.compile(regexPattern)
                .matcher(value)
                .matches();
    }

    private static Email sendRegistrationEmail(PersonDto personDto) {
        Email email = new Email();
        email.setEmail(personDto.getEmail());
        email.setSubject("Registration successful");
        email.setText("You have successfully created an account: "
                + personDto.getUsername() + " at localhost.");

        return email;
    }
}
