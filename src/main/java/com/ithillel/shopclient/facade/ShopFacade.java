package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.ShopDto;
import org.springframework.ui.Model;

import java.security.Principal;
import java.util.List;

public interface ShopFacade {

    void createShop(ShopDto shopDto);

    ShopDto findShopById(String id);

    List<ShopDto> getAllShops();

    void getAllProducts(String id, Model model);

    void addToCartForm(String shop, String id, Model model, Principal principal);

    void addProductToCart(CartDto cartDto);

    void updateShop(ShopDto shopDto);

    void deleteShopById(String id);

}
