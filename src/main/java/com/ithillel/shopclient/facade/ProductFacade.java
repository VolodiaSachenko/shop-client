package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;

import java.util.List;

public interface ProductFacade {

    ProductDto save(ProductDto productDto);

    ProductDto findProductById(String id);

    List<ProductDto> getAllProducts();

    void updateProduct(ProductDto productDto);

    void deleteProductById(String id);

    List<ShopDto> addProductForm();

}
