package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;

import java.util.List;
import java.util.Optional;

public interface ShopService {

    void createShop(Shop shop);

    Shop findShopById(Long id);

    List<Shop> getAllShops();

    List<Product> getAllProducts(Shop shop);

    void updateShop(Shop shop);

    void deleteShopById(Long id);

    void addProductToCart(Long personId, Long productId, Long shopId);

    void deleteProductFromCart(Long personId, Long productId, Long shopId);

    Optional<Shop> findByName(String name);

}
