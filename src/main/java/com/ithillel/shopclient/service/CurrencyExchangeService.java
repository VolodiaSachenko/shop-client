package com.ithillel.shopclient.service;

import java.math.BigDecimal;

public interface CurrencyExchangeService {

    BigDecimal exchangeToEuros(BigDecimal sum);

    BigDecimal exchangeToDollars(BigDecimal sum);

}
