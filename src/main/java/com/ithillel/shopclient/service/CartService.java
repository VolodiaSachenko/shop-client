package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.Cart;

import java.util.List;

public interface CartService {

    Cart save(Cart cart);

    Cart findById(Long id);

    List<Cart> getAll();

    Cart update(Cart cart);

    void deleteById(Long id);

}
