package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.CurrencyExchange;
import com.ithillel.shopclient.rest.client.PrivateRestClient;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Objects;

@Service
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
    private final PrivateRestClient privateRestClient;

    public CurrencyExchangeServiceImpl(PrivateRestClient privateRestClient) {
        this.privateRestClient = privateRestClient;
    }

    @Override
    public BigDecimal exchangeToEuros(BigDecimal sum) {

        return exchange("EUR", sum);
    }

    @Override
    public BigDecimal exchangeToDollars(BigDecimal sum) {

        return exchange("USD", sum);
    }

    private BigDecimal exchange(String ccy, BigDecimal sum) {
        CurrencyExchange[] currencyExchange = (CurrencyExchange[]) privateRestClient.getObjectsFromJson();
        CurrencyExchange current = Arrays.stream(currencyExchange)
                .filter(currency -> Objects.equals(currency.getCcy(), ccy))
                .findFirst()
                .orElse(null);
        return sum.divide(Objects.requireNonNull(current).getSale(), RoundingMode.HALF_EVEN);
    }
}
