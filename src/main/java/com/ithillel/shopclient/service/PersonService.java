package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.Person;

import java.util.List;

public interface PersonService {

    Person save(Person person);

    Person findPersonById(Long id);

    Person findByEmail(String email);

    Person findByUsername(String login);

    List<Person> getAllPersons();

    void updatePerson(Person person);

    void deletePersonById(Long id);

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

    boolean existsByUsername(String username);

}
