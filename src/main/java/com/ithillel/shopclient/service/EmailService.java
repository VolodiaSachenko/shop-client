package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.Email;

import javax.mail.MessagingException;

public interface EmailService {
    void sendEmail(Email email) throws MessagingException;
}
