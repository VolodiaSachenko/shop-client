package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.Product;

import java.util.List;

public interface ProductService {

    Product save(Product product);

    Product findProductById(Long id);

    List<Product> getAllProducts();

    void updateProduct(Product product);

    void deleteProductById(Long id);

}
