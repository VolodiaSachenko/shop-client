package com.ithillel.shopclient.service;

import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.repository.CartRepository;
import com.ithillel.shopclient.repository.PersonRepository;
import com.ithillel.shopclient.repository.ShopRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class CartServiceImpl implements CartService {
    private final CartRepository cartRepository;
    private final ShopRepository shopRepository;
    private final PersonRepository personRepository;

    public CartServiceImpl(CartRepository cartRepository, ShopRepository shopRepository,
                           PersonRepository personRepository) {
        this.cartRepository = cartRepository;
        this.shopRepository = shopRepository;
        this.personRepository = personRepository;
    }

    @Transactional
    @Override
    public Cart save(Cart cart) {
        if (cart.getName() == null) {
            log.error("Cart name is null, at Cart save(Cart cart)");
            throw new InvalidRequestException("Name is required.");
        }
        Person person = personRepository.findByEmail(cart.getPersonId().getEmail())
                .orElseThrow(() ->
                        new InvalidRequestException("Invalid person email: " + cart.getPersonId().getEmail()));
        if (person.getCarts().stream()
                .noneMatch(personCart ->
                        Objects.equals(personCart.getShopId().getName(), cart.getShopId().getName()))) {

            cart.setPersonId(person);
            cart.setShopId(shopRepository.findByName(cart.getShopId().getName())
                    .orElseThrow(() ->
                            new InvalidRequestException("Invalid shop id: " + cart.getShopId().getId())));
            cart.setOrderDate(new Date());
            cart.setProducts(new ArrayList<>());
            cart.setSum(BigDecimal.ZERO);
        } else {
            log.error("Exception at Cart save() " + cart.getName() + " is already exists.");
            throw new InvalidRequestException("Cart already exists");
        }
        log.info("Cart " + cart.getName() + " successfully created");
        return cartRepository.save(cart);
    }

    @Override
    public Cart findById(Long id) {
        return cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The cart with id " + id + " doesn't exist."));
    }

    @Override
    public List<Cart> getAll() {
        return (List<Cart>) cartRepository.findAll();
    }

    @Override
    public Cart update(Cart cart) {
        if (cart.getName() == null) {
            log.error("Exception at Cart update(). Cart name is null");
            throw new InvalidRequestException("Name is required.");
        }
        Cart update = cartRepository.findById(cart.getId())
                .orElseThrow(() -> new NotFoundException("The cart with id " + cart.getId() + " doesn't exist."));
        update.setName(cart.getName());
        log.info("Cart " + update.getName() + " successfully updated.");
        return cartRepository.save(update);
    }

    @Override
    public void deleteById(Long id) {
        if (!cartRepository.existsById(id)) {
            log.warn("Cart id " + id + " is not valid. At deleteById(Long id)");
            throw new InvalidRequestException("Wrong id");
        }
        cartRepository.deleteById(id);
        log.info("Cart with id" + id + " successfully deleted.");
    }
}
