package com.ithillel.shopclient.service;

import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.repository.PersonRepository;
import com.ithillel.shopclient.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Slf4j
@Service
public class PersonServiceImpl implements PersonService {
    private final PersonRepository personRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public PersonServiceImpl(PersonRepository personRepository, RoleRepository roleRepository,
                             BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.personRepository = personRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public Person save(Person person) {
        person.setPassword(bCryptPasswordEncoder.encode(person.getPassword()));
        if (person.getUsername().equals("admin")) {
            person.setRoles(new HashSet<>(Collections.singleton(roleRepository.findById(2L)
                    .orElseThrow(() -> new NotFoundException("The ROLE_ADMIN doesn't exist")))));
        } else {
            person.setRoles(new HashSet<>(Collections.singleton(roleRepository.findById(1L)
                    .orElseThrow(() -> new NotFoundException("The ROLE_CUSTOMER doesn't exist")))));
        }
        log.info("Person " + person.getUsername() + " successfully created");
        return personRepository.save(person);
    }

    @Override
    public Person findPersonById(Long id) {
        return personRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The person with id " + id + " doesn't exist."));
    }

    @Override
    public Person findByEmail(String email) {
        return personRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException("The person with email " + email + " doesn't exist."));
    }

    @Override
    public Person findByUsername(String login) {
        return personRepository.findByUsername(login)
                .orElseThrow(() -> new NotFoundException("The person with login " + login + " doesn't exist."));
    }

    @Override
    public List<Person> getAllPersons() {
        return (List<Person>) personRepository.findAll();
    }

    @Override
    public void updatePerson(Person person) {
        Person update = personRepository.findById(person.getId())
                .orElseThrow(() ->
                        new NotFoundException("The person with id " + person.getId() + " doesn't exist."));
        update.setFirstName(person.getFirstName());
        update.setSurName(person.getSurName());
        update.setPhone(person.getPhone());
        update.setEmail(person.getEmail());

        personRepository.save(update);
        log.info("Person " + update.getUsername() + " successfully updated.");
    }

    @Override
    public void deletePersonById(Long id) {
        if (!personRepository.existsById(id)) {
            log.warn("Person id " + id + " is not valid. At deletePersonById(Long id)");
            throw new InvalidRequestException("The person with id: " + id + " doesn't exist.");
        }

        personRepository.deleteById(id);
        log.info("Person with id" + id + " successfully deleted.");
    }

    @Override
    public boolean existsByEmail(String email) {
        return personRepository.existsByEmail(email);
    }

    @Override
    public boolean existsByPhone(String phone) {
        return personRepository.existsByPhone(phone);
    }

    @Override
    public boolean existsByUsername(String username) {
        return personRepository.existsByUsername(username);
    }
}
