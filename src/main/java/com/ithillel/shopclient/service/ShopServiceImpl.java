package com.ithillel.shopclient.service;

import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.repository.CartRepository;
import com.ithillel.shopclient.repository.PersonRepository;
import com.ithillel.shopclient.repository.ProductRepository;
import com.ithillel.shopclient.repository.ShopRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class ShopServiceImpl implements ShopService {
    private final ShopRepository shopRepository;
    private final ProductRepository productRepository;
    private final CartRepository cartRepository;
    private final PersonRepository personRepository;

    public ShopServiceImpl(ShopRepository shopRepository, ProductRepository productRepository,
                           CartRepository cartRepository, PersonRepository personRepository) {
        this.shopRepository = shopRepository;
        this.productRepository = productRepository;
        this.cartRepository = cartRepository;
        this.personRepository = personRepository;
    }

    @Override
    public void createShop(Shop shop) {
        checkRequest(shop);
        if (shop.getProducts() == null) {
            shop.setProducts(new ArrayList<>());
        }
        shopRepository.save(shop);
        log.info("Shop " + shop.getName() + " successfully created");
    }

    @Override
    public Shop findShopById(Long id) {
        return shopRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Invalid shop id: " + id));
    }

    @Override
    public List<Shop> getAllShops() {
        return (List<Shop>) shopRepository.findAll();
    }

    @Override
    public List<Product> getAllProducts(Shop shop) {
        if (shop.getId() == null) {
            log.error("Shop id is null, at List<Product> getAllProducts(Shop shop)");
            throw new InvalidRequestException("Shop id is required.");
        }
        return productRepository.findByShopId(shop)
                .orElseThrow(() -> new NotFoundException("Invalid shop id: " + shop.getId()));
    }

    @Override
    public void updateShop(Shop shop) {
        checkRequest(shop);
        Shop update = shopRepository.findById(shop.getId())
                .orElseThrow(() -> new NotFoundException("The shop with id " + shop.getId() + " doesn't exist."));
        update.setName(shop.getName());
        update.setDescription(shop.getDescription());
        update.setPhone(shop.getPhone());
        update.setRating(shop.getRating());

        shopRepository.save(update);
        log.info("Shop " + update.getName() + " successfully updated.");
    }

    @Transactional
    @Override
    public void deleteShopById(Long id) {
        if (!shopRepository.existsById(id)) {
            log.warn("Shop id " + id + " is not valid. At deleteShopById(Long id)");
            throw new InvalidRequestException("Invalid shop id " + id);
        }
        List<Cart> carts = (List<Cart>) cartRepository.findAll();
        if (carts.size() > 0) {
            for (Cart cart : carts) {
                if (cart.getShopId() != null && Objects.equals(cart.getShopId().getId(), id)) {
                    cartRepository.deleteById(cart.getId());
                }
            }
        }
        shopRepository.deleteById(id);
        log.info("Shop with id" + id + " successfully deleted.");
    }

    @Transactional
    @Override
    public void addProductToCart(Long personId, Long productId, Long shopId) {
        Person persistPerson = personRepository.findById(personId)
                .orElseThrow(() -> new NotFoundException("The person with id " + personId + " doesn't exist."));
        if (persistPerson.getCarts() == null) {
            persistPerson.setCarts(new ArrayList<>());
        }
        Shop shop = shopRepository.findById(shopId)
                .orElseThrow(() -> new NotFoundException("The shop with id " + shopId + " doesn't exist."));

        Product shopProduct = productRepository.findById(productId)
                .orElseThrow(() ->
                        new NotFoundException("The product with id " + productId + " doesn't exist."));
        checkProductKey(shopProduct.getShopId().getId(), shop.getId());
        checkCarts(persistPerson, shop, shopId);
        addToPersonCart(persistPerson, shopProduct, shop);

        personRepository.save(persistPerson);
        log.info("Product " + shopProduct.getName() + " successfully added to cart.");
    }

    @Transactional
    @Override
    public void deleteProductFromCart(Long personId, Long productId, Long shopId) {
        Person persistPerson = personRepository.findById(personId)
                .orElseThrow(() -> new NotFoundException("The person with id " + personId + " doesn't exist."));
        Product personProduct = productRepository.findById(productId)
                .orElseThrow(() ->
                        new NotFoundException("The product with id " + productId + " doesn't exist."));
        checkProductKey(personProduct.getShopId().getId(), shopId);

        persistPerson.getCarts()
                .stream()
                .filter(cart -> Objects.equals(cart.getShopId().getId(), shopId))
                .findFirst()
                .ifPresent(cart -> {
                    if (cart.getProducts().size() != 0) {
                        if (cart.getProducts().remove(personProduct)) {
                            cart.setSum(cart.getSum().subtract(personProduct.getPrice()));
                        } else {
                            log.error("Product doesnt added to cart. " +
                                    "At deleteProductFromCart(Long personId, Long productId, Long shopId)");
                            throw new InvalidRequestException("This product is not added to the cart.");
                        }
                    } else {
                        throw new InvalidRequestException("Cart is empty");
                    }
                });
        personRepository.save(persistPerson);
        log.info("Product " + personProduct.getName() + " successfully deleted from cart.");
    }

    @Override
    public Optional<Shop> findByName(String name) {
        return shopRepository.findByName(name);
    }

    private Cart createCart(Person person, Shop shop) {
        Cart cart = new Cart();
        cart.setProducts(new ArrayList<>());
        cart.setShopId(shop);
        cart.setPersonId(person);
        cart.setOrderDate(new Date());
        cart.setSum(BigDecimal.ZERO);
        cart.setName(shop.getName() + " cart");

        return cartRepository.save(cart);
    }

    private static void checkRequest(Shop shop) {
        if (shop.getName() == null || shop.getDescription() == null || shop.getPhone() == null) {
            log.error("Invalid Shop fields");
            throw new InvalidRequestException("Fields name, description and phone are required.");
        }
    }

    private void checkCarts(Person persistPerson, Shop shop, Long shopId) {
        boolean isCartPresent = persistPerson.getCarts().stream()
                .anyMatch(cart -> Objects.equals(cart.getShopId().getId(), shopId));
        if (!isCartPresent) {
            persistPerson.getCarts().add(createCart(persistPerson, shop));
        }
    }

    private static void addToPersonCart(Person persistPerson, Product shopProduct, Shop shop) {
        persistPerson.getCarts().stream()
                .filter(cart -> Objects.equals(cart.getShopId().getId(), shop.getId()))
                .findFirst()
                .ifPresent(cart -> {
                    cart.getProducts().add(shopProduct);
                    cart.setSum(cart.getSum().add(shopProduct.getPrice()));
                });
    }

    private static void checkProductKey(Long productShopId, Long shopId) {
        if (productShopId.longValue() != shopId.longValue()) {
            log.warn("Cannot add product from other shop.");
            throw new InvalidRequestException("This product belongs to other shop.");
        }
    }
}
