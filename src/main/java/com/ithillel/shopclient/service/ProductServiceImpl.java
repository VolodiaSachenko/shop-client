package com.ithillel.shopclient.service;

import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.repository.CartRepository;
import com.ithillel.shopclient.repository.ProductRepository;
import com.ithillel.shopclient.repository.ShopRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ShopRepository shopRepository;
    private final CartRepository cartRepository;

    public ProductServiceImpl(ProductRepository productRepository, ShopRepository shopRepository,
                              CartRepository cartRepository) {
        this.productRepository = productRepository;
        this.shopRepository = shopRepository;
        this.cartRepository = cartRepository;
    }

    @Override
    public Product save(Product product) {
        checkRequest(product);
        product.setShopId(shopRepository.findByName(product.getShopId().getName())
                .orElseThrow(() -> new NotFoundException("The shop with name " + product.getShopId().getName() +
                        " doesn't exist.")));

        log.info("Product " + product.getName() + " successfully created");
        return productRepository.save(product);
    }

    @Override
    public Product findProductById(Long id) {

        return productRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("The product with id " + id + " doesn't exist."));
    }

    @Override
    public List<Product> getAllProducts() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public void updateProduct(Product product) {
        checkRequest(product);
        Product update = productRepository.findById(product.getId())
                .orElseThrow(() -> new NotFoundException("Invalid product id " + product.getId()));
        update.setName(product.getName());
        update.setDescription(product.getDescription());
        update.setPrice(product.getPrice());

        productRepository.save(update);
        log.info("Product " + update.getName() + " successfully updated.");
    }

    @Transactional
    @Override
    public void deleteProductById(Long productId) {
        if (!productRepository.existsById(productId)) {
            log.warn("Product id " + productId + " is not valid. At deleteProductById(Long id)");
            throw new InvalidRequestException("Invalid product id: " + productId);
        }
        List<Cart> carts = (List<Cart>) cartRepository.findAll();
        Product removeProduct = productRepository.findById(productId)
                .orElseThrow(() -> new NotFoundException("Product doesn't exist"));

        for (Cart cart : carts) {
            for (int i = 0; i < cart.getProducts().size(); i++) {
                if (Objects.equals(cart.getProducts().get(i).getId(), productId)) {
                    cart.setSum(cart.getSum().subtract(removeProduct.getPrice()));
                }
            }
            cart.getProducts().removeIf(product -> Objects.equals(product.getId(), productId));
            cartRepository.save(cart);
        }
        productRepository.deleteById(productId);
        log.info("Product with id" + productId + " successfully deleted.");
    }

    private static void checkRequest(Product product) {
        if (product.getName() == null || product.getPrice() == null) {
            log.error("Invalid product fields values: " +
                    "name: " + product.getName() + " price: " + product.getPrice());
            throw new InvalidRequestException("Fields name, shopId and price are required.");
        }
    }
}
