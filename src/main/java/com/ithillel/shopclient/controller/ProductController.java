package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.facade.ProductFacade;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
public class ProductController {
    private final ProductFacade productFacade;

    public ProductController(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/product/add"}, method = RequestMethod.GET)
    public String addProductForm(Model model) {
        List<ShopDto> shops = productFacade.addProductForm();
        model.addAttribute("shops", shops);
        model.addAttribute("product", new ProductDto());
        return "addProduct";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/product/add/create"}, method = RequestMethod.POST)
    public ModelAndView createProduct(@ModelAttribute("product") ProductDto product) {
        productFacade.save(product);
        return new ModelAndView(new RedirectView("/product"));
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/product"}, method = RequestMethod.GET)
    public String getAllProducts(Model model) {
        List<ProductDto> products = productFacade.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/product/update/id{id}"}, method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable String id, Model model) {
        model.addAttribute("updateProduct", productFacade.findProductById(id));
        return "updateProduct";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/product/update/confirm"}, method = RequestMethod.POST)
    public ModelAndView updateProduct(@ModelAttribute("updateProduct") ProductDto product) {
        productFacade.updateProduct(product);
        return new ModelAndView(new RedirectView("/product"));
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/product/delete/id{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteProduct(@PathVariable String id) {
        productFacade.deleteProductById(id);
        return new ModelAndView(new RedirectView("/product"));
    }
}
