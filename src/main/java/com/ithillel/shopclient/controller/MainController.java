package com.ithillel.shopclient.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
public class MainController {

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Principal principal, Model model) {
        model.addAttribute("principal", principal.getName());
        return "index";
    }
}
