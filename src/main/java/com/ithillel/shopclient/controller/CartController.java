package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.facade.CartFacade;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
public class CartController {
    private final CartFacade cartFacade;

    public CartController(CartFacade cartFacade) {
        this.cartFacade = cartFacade;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/cart/add"}, method = RequestMethod.GET)
    public String addCartForm(Model model) {
        cartFacade.addCartForm(model);
        return "addCart";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/cart/add/create"}, method = RequestMethod.POST)
    public ModelAndView createCart(@ModelAttribute("cart") CartDto cart) {
        cartFacade.save(cart);
        return new ModelAndView(new RedirectView("/cart"));
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"/cart"}, method = RequestMethod.GET)
    public String getAllCarts(Model model, Principal principal, HttpServletRequest request) {
        List<CartDto> carts = cartFacade.getAll(principal, request);
        model.addAttribute("carts", carts);
        model.addAttribute("principal", principal);
        return "carts";
    }

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @RequestMapping(value = {"/cart/id{id}/products"}, method = RequestMethod.GET)
    public String getAllProducts(@PathVariable String id, Model model) {
        CartDto cartDto = cartFacade.findById(id);
        model.addAttribute("cartDto", cartDto);
        return "cartProducts";
    }

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @RequestMapping(value = {"/cart/update/id{id}"}, method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable String id, Model model) {
        model.addAttribute("updateCart", cartFacade.findById(id));
        return "updateCart";
    }

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @RequestMapping(value = {"/cart/update/confirm"}, method = RequestMethod.POST)
    public ModelAndView updateCart(@ModelAttribute("updateCart") CartDto cart) {
        cartFacade.update(cart);
        return new ModelAndView(new RedirectView("/cart"));
    }

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @RequestMapping(value = {"/cart/delete/id{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteCart(@PathVariable String id) {
        cartFacade.deleteById(id);
        return new ModelAndView(new RedirectView("/cart"));
    }

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @RequestMapping(value = {"/cart/delete/id{id}/{cartDto}"}, method = RequestMethod.POST)
    public ModelAndView deleteProduct(@PathVariable String id, @PathVariable String cartDto) {
        cartFacade.deleteProduct(id, cartDto);
        return new ModelAndView(new RedirectView("/cart"));
    }
}
