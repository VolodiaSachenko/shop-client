package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.facade.ShopFacade;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.security.Principal;
import java.util.List;

@Controller
public class ShopController {
    private final ShopFacade shopFacade;

    public ShopController(ShopFacade shopFacade) {
        this.shopFacade = shopFacade;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/shop/add"}, method = RequestMethod.GET)
    public String addShopForm(Model model) {
        model.addAttribute("shop", new ShopDto());
        return "addShop";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/shop/add/create"}, method = RequestMethod.POST)
    public ModelAndView createShop(@ModelAttribute("shop") ShopDto shop) {
        shopFacade.createShop(shop);
        return new ModelAndView(new RedirectView("/shop"));
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"/shop"}, method = RequestMethod.GET)
    public String getAllShops(Model model) {
        List<ShopDto> shops = shopFacade.getAllShops();
        model.addAttribute("shops", shops);
        return "shops";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/shop/update/id{id}"}, method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable String id, Model model) {
        model.addAttribute("updateShop", shopFacade.findShopById(id));
        return "updateShop";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/shop/update/confirm"}, method = RequestMethod.POST)
    public ModelAndView updateShop(@ModelAttribute("updateShop") ShopDto shop) {
        shopFacade.updateShop(shop);
        return new ModelAndView(new RedirectView("/shop"));
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"/shop/delete/id{id}"}, method = RequestMethod.GET)
    public ModelAndView deleteShop(@PathVariable String id) {
        shopFacade.deleteShopById(id);
        return new ModelAndView(new RedirectView("/shop"));
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"/shop/id{id}/products"}, method = RequestMethod.GET)
    public String getAllShopProducts(@PathVariable String id, Model model) {
        shopFacade.getAllProducts(id, model);
        return "shopProducts";
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"/shop/id{shop}/add-to-cart/product-id{id}"}, method = RequestMethod.GET)
    public String addToCartForm(@PathVariable String shop, @PathVariable String id,
                                Model model, Principal principal) {
        shopFacade.addToCartForm(shop, id, model, principal);
        return "addToCart";
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"/confirm-order"}, method = RequestMethod.POST)
    public ModelAndView addToCart(@ModelAttribute("order") CartDto cartDto) {
        shopFacade.addProductToCart(cartDto);
        return new ModelAndView(new RedirectView("/shop"));
    }
}
