package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.facade.PersonFacade;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller
public class PersonController {
    private final PersonFacade personFacade;

    public PersonController(PersonFacade personFacade) {
        this.personFacade = personFacade;
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("login", new PersonDto());
        return "login";
    }

    @RequestMapping(value = {"/login/confirm"}, method = RequestMethod.POST)
    public String confirmLogin(@ModelAttribute("login") PersonDto personDto,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        return "redirect:/";
    }

    @RequestMapping(value = {"/registration"}, method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("registrationForm", new PersonDto());
        return "registration";
    }

    @RequestMapping(value = {"/registration/confirm"}, method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("registrationForm") PersonDto personDto,
                               BindingResult bindingResult, Model model) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (!personDto.getPassword().equals(personDto.getPasswordConfirm())) {
            model.addAttribute("error", "Wrong password confirmation.");
            return "registration";
        }
        personFacade.save(personDto);
        return "redirect:/";
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"/person"}, method = RequestMethod.GET)
    public String allPersons(Model model, Principal principal, HttpServletRequest request) {
        List<PersonDto> persons = personFacade.getAllPersons(principal, request);
        model.addAttribute("principal", principal);
        model.addAttribute("persons", persons);
        return "persons";
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"person/update/id{id}"}, method = RequestMethod.GET)
    public String showUpdateForm(@PathVariable String id, Model model) {
        model.addAttribute("update", personFacade.findPersonById(id));
        return "updatePerson";
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"person/edit"}, method = RequestMethod.GET)
    public String editPerson(Model model, Principal principal) {
        model.addAttribute("update", personFacade.findByUsername(principal.getName()));
        return "updatePerson";
    }

    @Secured({"ROLE_ADMIN", "ROLE_CUSTOMER"})
    @RequestMapping(value = {"person/update/confirm"}, method = RequestMethod.POST)
    public ModelAndView updatePerson(@ModelAttribute("update") PersonDto person, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView(new RedirectView("/error"));
        }
        personFacade.updatePerson(person);
        return new ModelAndView(new RedirectView("/person"));
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = {"person/delete/id{id}"}, method = RequestMethod.GET)
    public ModelAndView deletePerson(@PathVariable String id) {
        personFacade.deletePersonById(id);
        return new ModelAndView(new RedirectView("/person"));
    }
}
