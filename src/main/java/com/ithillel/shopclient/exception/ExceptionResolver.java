package com.ithillel.shopclient.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionResolver {

    @ExceptionHandler(value = InvalidRequestException.class)
    public ResponseEntity<?> invalidRequestHandler() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<?> notFoundHandler() {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ValidateException.class)
    public ResponseEntity<?> validateHandler() {
        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
    }

}
