package com.ithillel.shopclient.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebSecurityConfig {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
        return new UrlAuthenticationSuccessHandler();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http
                .csrf().disable()
                .authorizeRequests()
                    .antMatchers("/").authenticated()
                    .antMatchers("/product/**").hasRole("ADMIN")
                    .antMatchers("/person/**").authenticated()
                    .antMatchers("/shop/**").authenticated()
                    .antMatchers("/cart/**").authenticated()
                    .antMatchers("/error*").permitAll()
                    .antMatchers("/registration/**").anonymous()
                    .antMatchers("/login*").anonymous()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login/confirm")
                    .successHandler(myAuthenticationSuccessHandler())
                    .failureUrl("/error")
                .and()
                    .logout()
                    .logoutUrl("/logout")
                    .deleteCookies("JSESSIONID");
        return http.build();
    }
}
