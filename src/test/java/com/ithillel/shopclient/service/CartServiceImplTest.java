package com.ithillel.shopclient.service;

import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.repository.CartRepository;

import com.ithillel.shopclient.repository.PersonRepository;
import com.ithillel.shopclient.repository.ShopRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createCart;
import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CartServiceImplTest {
    private Cart cart;
    @Mock
    private CartRepository cartRepository;
    @Mock
    private ShopRepository shopRepository;
    @Mock
    private PersonRepository personRepository;
    @InjectMocks
    private CartServiceImpl cartService;

    private MockitoSession session;

    @Before
    public void setUp() {
        Shop shop = createShop();
        Person person = createPerson();
        Product product = createProduct(shop);
        this.cart = createCart(shop, person);
        this.cart.getProducts().add(product);
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void findAll() {
        List<Cart> list = new ArrayList<>();
        list.add(cart);

        when(cartRepository.findAll()).thenReturn(list);

        List<Cart> serviceList = cartService.getAll();

        assertEquals(list, serviceList);
        verify(cartRepository, times(1)).findAll();
    }

    @Test
    public void save() {
        mockRepositoryCheckLogic();

        when(cartRepository.save(any())).thenReturn(cart);

        Cart saveCart = cartService.save(cart);

        checkCartFields(cart, saveCart);
        verify(cartRepository, times(1)).save(cart);
    }

    @Test
    public void saveThrowsInvalidRequestException() {
        cart.setName(null);

        Throwable requestException = assertThrows(InvalidRequestException.class, () -> cartService.save(cart));

        assertEquals("Name is required.", requestException.getMessage());
    }

    @Test
    public void findById() {
        when(cartRepository.findById(anyLong())).thenReturn(Optional.ofNullable(cart));

        Cart getCart = cartService.findById(1L);

        checkCartFields(cart, getCart);
        verify(cartRepository, times(1)).findById(1L);
    }

    @Test
    public void findByIdThrowsNotFoundException() {
        cart.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                cartService.findById(cart.getId()));
        assertEquals("The cart with id " + cart.getId() +
                " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void update() {
        mockRepositoryCheckLogic();

        when(cartRepository.save(any())).thenReturn(cart);

        Cart update = cartService.save(cart);

        checkCartFields(cart, update);
        verify(cartRepository, times(1)).save(cart);
    }

    @Test
    public void updateThrowsNotFoundException() {
        cart.setId(110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                cartService.update(cart));
        assertEquals("The cart with id " +
                cart.getId() + " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void updateThrowsInvalidRequestException() {
        cart.setName(null);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                cartService.update(cart));
        assertEquals("Name is required.", requestException.getMessage());
    }

    @Test
    public void deleteById() {
        when(cartRepository.existsById(anyLong())).thenReturn(true);
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Cart " + cart.getName() + " successfully deleted");
                    return null;
                }
        ).when(cartRepository).deleteById(anyLong());
        cartService.deleteById(cart.getId());
        verify(cartRepository, times(1)).deleteById(cart.getId());
    }

    @Test
    public void deleteByIdThrowsInvalidRequestException() {
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                cartService.deleteById(100L));
        assertEquals("Wrong id", requestException.getMessage());
    }

    private static void checkCartFields(Cart expected, Cart actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getSum(), actual.getSum());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getShopId(), actual.getShopId());
        assertEquals(expected.getPersonId(), actual.getPersonId());
        assertEquals(expected.getProducts(), actual.getProducts());
        assertEquals(expected.getOrderDate(), actual.getOrderDate());
    }

    private void mockRepositoryCheckLogic() {
        when(personRepository.findByEmail(cart.getPersonId().getEmail()))
                .thenReturn(Optional.ofNullable(cart.getPersonId()));
        when(shopRepository.findByName(cart.getShopId().getName()))
                .thenReturn(Optional.ofNullable(cart.getShopId()));
    }
}