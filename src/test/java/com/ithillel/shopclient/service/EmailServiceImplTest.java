package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.Email;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import static com.ithillel.shopclient.ModelsFactory.createEmail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class EmailServiceImplTest {
    private Email email;
    @Mock
    private JavaMailSender javaMailSender;
    @InjectMocks
    private EmailServiceImpl javaMailSenderService;

    private MockitoSession session;

    @Before
    public void setUp() {
        this.email = createEmail();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void sendEmail() throws MessagingException {
        when(javaMailSender.createMimeMessage()).thenReturn(mock(MimeMessage.class));

        javaMailSenderService.sendEmail(email);

        verify(javaMailSender, times(1)).createMimeMessage();
    }
}