package com.ithillel.shopclient.service;

import com.ithillel.shopclient.ModelsFactory;
import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;

import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.repository.CartRepository;
import com.ithillel.shopclient.repository.PersonRepository;
import com.ithillel.shopclient.repository.ProductRepository;
import com.ithillel.shopclient.repository.ShopRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createCart;
import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ShopServiceImplTest {
    private Shop shop;
    private Person person;
    private Product product;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ShopRepository shopRepository;
    @Mock
    private PersonRepository personRepository;
    @Mock
    private CartRepository cartRepository;
    @InjectMocks
    private ShopServiceImpl shopService;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.shop = ModelsFactory.createShop();
        this.person = createPerson();
        this.product = createProduct(shop);
        this.shop.getProducts().add(product);

        Cart cart = createCart(shop, person);
        cart.getProducts().add(product);

        this.person.getCarts().add(cart);
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void createShop() {
        when(shopRepository.save(any())).thenReturn(shop);

        shopService.createShop(shop);

        verify(shopRepository, times(1)).save(shop);
    }

    @Test
    public void createShopThrowsInvalidRequestException() {
        shop.setName(null);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                shopService.createShop(shop));
        assertEquals("Fields name, description and phone are required.", requestException.getMessage());
    }

    @Test
    public void findShopById() {
        when(shopRepository.findById(anyLong())).thenReturn(Optional.of(shop));

        Shop findShop = shopService.findShopById(shop.getId());

        checkShopFields(shop, findShop);
        verify(shopRepository, times(1)).findById(shop.getId());
    }

    @Test
    public void findShopByIdThrowsNotFoundException() {
        shop.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.findShopById(shop.getId()));
        assertEquals("Invalid shop id: " + shop.getId(), requestException.getMessage());
    }

    @Test
    public void getAllShops() {
        List<Shop> shops = new ArrayList<>();
        shops.add(shop);

        when(shopRepository.findAll()).thenReturn(shops);

        List<Shop> shopServiceList = shopService.getAllShops();

        assertEquals(shops, shopServiceList);
        verify(shopRepository, times(1)).findAll();
    }

    @Test
    public void getAllProducts() {
        when(productRepository.findByShopId(any())).thenReturn(Optional.of(shop.getProducts()));

        List<Product> serviceList = shopService.getAllProducts(shop);

        assertEquals(shop.getProducts(), serviceList);
        verify(productRepository, times(1)).findByShopId(any());
    }

    @Test
    public void getAllProductsThrowsInvalidRequestException() {
        shop.setId(null);
        Throwable exception = assertThrows(InvalidRequestException.class, () ->
                shopService.getAllProducts(shop));
        assertEquals("Shop id is required.", exception.getMessage());
    }

    @Test
    public void getAllProductsThrowsNotFoundException() {
        shop.setId(1100L);
        Throwable exception = assertThrows(NotFoundException.class, () ->
                shopService.getAllProducts(shop));
        assertEquals("Invalid shop id: " + shop.getId(), exception.getMessage());
    }

    @Test
    public void updateShop() {
        when(shopRepository.findById(anyLong())).thenReturn(Optional.of(shop));
        when(shopRepository.save(any())).thenReturn(shop);

        shopService.updateShop(shop);

        verify(shopRepository, times(1)).save(shop);
    }

    @Test
    public void updateShopThrowsNotFoundException() {
        shop.setId(1110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.updateShop(shop));
        assertEquals("The shop with id " + shop.getId() +
                " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void deleteShopById() {
        when(shopRepository.existsById(anyLong())).thenReturn(true);
        when(cartRepository.findAll()).thenReturn(new ArrayList<>());
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shop.getName() + " successfully deleted");
                    return null;
                }
        ).when(shopRepository).deleteById(anyLong());

        shopService.deleteShopById(shop.getId());

        verify(shopRepository, times(1)).deleteById(shop.getId());
    }

    @Test
    public void deleteShopByIdThrowsInvalidRequestException() {
        shop.setId(100L);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                shopService.deleteShopById(shop.getId()));
        assertEquals("Invalid shop id " + shop.getId(), requestException.getMessage());
    }

    @Test
    public void addProductToCart() {
        mockRepositoryLogic(person, shop, product);

        shopService.addProductToCart(person.getId(), product.getId(), shop.getId());

        verify(personRepository, times(1))
                .save(person);
    }

    @Test
    public void addProductToCartThrowsNotFoundExceptionPerson() {
        person.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.addProductToCart(person.getId(), 1L, 1L));
        assertEquals("The person with id " +
                person.getId() + " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void addProductToCartThrowsNotFoundExceptionShop() {
        shop.setId(100L);
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(new Person()));
        Throwable requestExceptionShop = assertThrows(NotFoundException.class, () ->
                shopService.addProductToCart(1L, 1L, shop.getId()));
        assertEquals("The shop with id " +
                shop.getId() + " doesn't exist.", requestExceptionShop.getMessage());
    }

    @Test
    public void addProductToCartThrowsNotFoundExceptionProduct() {
        product.setId(100L);
        when(shopRepository.findById(anyLong())).thenReturn(Optional.of(new Shop()));
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(new Person()));
        Throwable requestExceptionProduct = assertThrows(NotFoundException.class, () ->
                shopService.addProductToCart(1L, product.getId(), 1L));
        assertEquals("The product with id " +
                product.getId() + " doesn't exist.", requestExceptionProduct.getMessage());
    }

    @Test
    public void deleteProductFromCart() {
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));
        when(personRepository.save(any())).thenReturn(person);

        shopService.deleteProductFromCart(1L, 1L, 1L);

        verify(personRepository, times(1)).save(any());
    }

    @Test
    public void deleteProductFromCartThrowsNotFoundException() {
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                shopService.deleteProductFromCart(100L, 1L, 1L));
        assertEquals("The person with id 100 doesn't exist.", requestException.getMessage());

        when(personRepository.findById(anyLong())).thenReturn(Optional.of(new Person()));
        Throwable requestExceptionProduct = assertThrows(NotFoundException.class, () ->
                shopService.deleteProductFromCart(1L, 100L, 1L));
        assertEquals("The product with id 100 doesn't exist.", requestExceptionProduct.getMessage());
    }

    @Test
    public void findByName() {
        when(shopRepository.findByName(anyString())).thenReturn(Optional.of(shop));

        Shop byName = shopService.findByName(shop.getName()).orElse(new Shop());

        checkShopFields(shop, byName);
        verify(shopRepository, times(1)).findByName(anyString());
    }

    private void mockRepositoryLogic(Person person, Shop shop, Product product) {
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));
        when(shopRepository.findById(anyLong())).thenReturn(Optional.of(shop));
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));
        when(personRepository.save(any())).thenReturn(person);
    }

    private static void checkShopFields(Shop expected, Shop actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getProducts(), actual.getProducts());
        assertEquals(expected.getRating(), actual.getRating());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getPhone(), actual.getPhone());
    }
}