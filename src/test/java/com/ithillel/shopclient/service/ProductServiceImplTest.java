package com.ithillel.shopclient.service;

import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.repository.CartRepository;

import com.ithillel.shopclient.repository.ProductRepository;
import com.ithillel.shopclient.repository.ShopRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductServiceImplTest {
    private Product product;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ShopRepository shopRepository;
    @Mock
    private CartRepository cartRepository;

    @InjectMocks
    private ProductServiceImpl productService;
    private MockitoSession session;

    @Before
    public void setUp() {
        Shop shop = createShop();
        this.product = createProduct(shop);
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(shopRepository.findByName(anyString())).thenReturn(Optional.of(product.getShopId()));
        when(productRepository.save(any())).thenReturn(product);

        Product saveProduct = productService.save(product);

        checkProductFields(product, saveProduct);
        verify(productRepository, times(1)).save(product);
    }

    @Test
    public void saveThrowsNotFoundException() {
        product.getShopId().setName(null);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                productService.save(product));
        assertEquals("The shop with name " + product.getShopId().getName() +
                " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void saveThrowsInvalidRequestException() {
        product.setName(null);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                productService.save(product));
        assertEquals("Fields name, shopId and price are required.", requestException.getMessage());
    }

    @Test
    public void findProductById() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));

        Product findProduct = productService.findProductById(anyLong());

        checkProductFields(product, findProduct);
        verify(productRepository, times(1)).findById(anyLong());
    }

    @Test
    public void findProductByIdThrowsNotFoundException() {
        product.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                productService.findProductById(product.getId()));
        assertEquals("The product with id " + product.getId() +
                " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void getAllProducts() {
        List<Product> products = new ArrayList<>();
        products.add(product);

        when(productRepository.findAll()).thenReturn(products);

        List<Product> serviceList = productService.getAllProducts();

        assertEquals(products, serviceList);
        verify(productRepository, times(1)).findAll();
    }

    @Test
    public void updateProduct() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));
        when(productRepository.save(any())).thenReturn(product);

        productService.updateProduct(product);

        verify(productRepository, times(1)).save(product);
    }

    @Test
    public void updateProductThrowsNotFoundException() {
        product.setId(110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                productService.updateProduct(product));
        assertEquals("Invalid product id " + product.getId(), requestException.getMessage());
    }

    @Test
    public void deleteProductById() {
        when(productRepository.existsById(anyLong())).thenReturn(true);
        when(cartRepository.findAll()).thenReturn(new ArrayList<>());
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product " + product.getName() + " successfully deleted");
                    return null;
                }
        ).when(productRepository).deleteById(anyLong());

        productService.deleteProductById(product.getId());

        verify(productRepository, times(1)).deleteById(product.getId());
    }

    @Test
    public void deleteProductByIdThrowsInvalidRequestException() {
        product.setId(100L);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                productService.deleteProductById(product.getId()));
        assertEquals("Invalid product id: " + product.getId(), requestException.getMessage());
    }

    private static void checkProductFields(Product expected, Product actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getShopId(), actual.getShopId());
        assertEquals(expected.getPrice(), actual.getPrice());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getDateCreate(), actual.getDateCreate());
    }
}