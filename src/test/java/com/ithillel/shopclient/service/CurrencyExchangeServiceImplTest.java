package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.CurrencyExchange;

import com.ithillel.shopclient.rest.client.PrivateRestClient;
import com.ithillel.shopclient.rest.client.PrivateRestClientImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static com.ithillel.shopclient.ModelsFactory.createCurrencyExchange;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CurrencyExchangeServiceImplTest {
    private CurrencyExchange currencyExchange;
    @Mock
    private PrivateRestClient privateRestClient;
    @InjectMocks
    private CurrencyExchangeServiceImpl currencyExchangeService;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.currencyExchange = createCurrencyExchange();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void exchangeToEuros() {
        currencyExchange.setCcy("EUR");
        mock(PrivateRestClientImpl.class);

        when(privateRestClient.getObjectsFromJson()).thenReturn(new CurrencyExchange[]{currencyExchange});
        currencyExchangeService.exchangeToEuros(new BigDecimal(0));

        verify(privateRestClient, times(1)).getObjectsFromJson();
    }

    @Test
    public void exchangeToDollars() {
        mock(PrivateRestClientImpl.class);

        when(privateRestClient.getObjectsFromJson()).thenReturn(new CurrencyExchange[]{currencyExchange});
        currencyExchangeService.exchangeToDollars(new BigDecimal(0));

        verify(privateRestClient, times(1)).getObjectsFromJson();
    }
}