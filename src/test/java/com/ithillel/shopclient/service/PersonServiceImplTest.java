package com.ithillel.shopclient.service;

import com.ithillel.shopclient.exception.InvalidRequestException;
import com.ithillel.shopclient.exception.NotFoundException;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Role;

import com.ithillel.shopclient.repository.PersonRepository;
import com.ithillel.shopclient.repository.RoleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PersonServiceImplTest {
    private Person person;
    @Mock
    private PersonRepository personRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @InjectMocks
    private PersonServiceImpl personService;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.person = createPerson();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn(person.getPassword());
        when(roleRepository.findById(anyLong())).thenReturn(Optional.of(new Role()));
        when(personRepository.save(any())).thenReturn(person);

        Person savePerson = personService.save(person);

        checkPersonFields(person, savePerson);
        verify(personRepository, times(1)).save(person);
    }

    @Test
    public void saveThrowsNotFoundException() {
        when(bCryptPasswordEncoder.encode(anyString())).thenReturn(person.getPassword());
        when(roleRepository.findById(anyLong())).thenThrow(new NotFoundException("The ROLE_CUSTOMER doesn't exist"));
        person.setRoles(new HashSet<>());
        person.getRoles().add(new Role());
        roleRepository.deleteAll();

        Throwable request = assertThrows(NotFoundException.class, () -> personService.save(person));
        assertEquals("The ROLE_CUSTOMER doesn't exist", request.getMessage());
    }

    @Test
    public void findPersonById() {
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));

        Person getPerson = personService.findPersonById(person.getId());

        checkPersonFields(person, getPerson);
        verify(personRepository, times(1)).findById(person.getId());
    }

    @Test
    public void findPersonByIdThrowsNotFoundException() {
        person.setId(100L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                personService.findPersonById(person.getId()));
        assertEquals("The person with id " +
                person.getId() + " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void findByEmail() {
        when(personRepository.findByEmail(anyString())).thenReturn(Optional.of(person));

        Person personByEmail = personService.findByEmail(person.getEmail());

        checkPersonFields(person, personByEmail);
        verify(personRepository, times(1)).findByEmail(person.getEmail());
    }

    @Test
    public void findByEmailThrowsNotFoundException() {
        person.setEmail(null);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                personService.findByEmail(person.getEmail()));
        assertEquals("The person with email " + person.getEmail() + " doesn't exist.",
                requestException.getMessage());
    }

    @Test
    public void findByUsername() {
        when(personRepository.findByUsername(anyString())).thenReturn(Optional.of(person));

        Person personByUsername = personService.findByUsername(person.getUsername());

        checkPersonFields(person, personByUsername);
        verify(personRepository, times(1)).findByUsername(person.getUsername());
    }

    @Test
    public void findByUsernameThrowsNotFoundException() {
        person.setUsername(null);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                personService.findByUsername(person.getUsername()));
        assertEquals("The person with login " + person.getUsername() + " doesn't exist.",
                requestException.getMessage());
    }

    @Test
    public void getAllPersons() {
        List<Person> list = new ArrayList<>();
        list.add(person);

        when(personRepository.findAll()).thenReturn(list);

        List<Person> serviceList = personService.getAllPersons();

        assertEquals(list, serviceList);
        verify(personRepository, times(1)).findAll();
    }

    @Test
    public void updatePerson() {
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));
        when(personRepository.save(any())).thenReturn(person);

        personService.updatePerson(person);

        verify(personRepository, times(1)).save(person);
    }

    @Test
    public void updatePersonThrowsNotFoundException() {
        person.setId(110L);
        Throwable requestException = assertThrows(NotFoundException.class, () ->
                personService.updatePerson(person));
        assertEquals("The person with id " +
                person.getId() + " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void deletePersonById() {
        when(personRepository.existsById(anyLong())).thenReturn(true);
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Person " + person.getUsername() + " successfully deleted");
                    return null;
                }
        ).when(personRepository).deleteById(anyLong());

        personService.deletePersonById(person.getId());

        verify(personRepository, times(1)).deleteById(person.getId());
    }

    @Test
    public void deletePersonByIdThrowsInvalidRequestException() {
        person.setId(110L);
        Throwable requestException = assertThrows(InvalidRequestException.class, () ->
                personService.deletePersonById(person.getId()));
        assertEquals("The person with id: " +
                person.getId() + " doesn't exist.", requestException.getMessage());
    }

    @Test
    public void existsByEmail() {
        when(personRepository.existsByEmail(anyString())).thenReturn(true);

        boolean isMailExists = personService.existsByEmail(person.getEmail());

        assertTrue(isMailExists);
        verify(personRepository, times(1)).existsByEmail(person.getEmail());
    }

    @Test
    public void existsByPhone() {
        when(personRepository.existsByPhone(anyString())).thenReturn(true);

        boolean isPhoneExists = personService.existsByPhone(person.getPhone());

        assertTrue(isPhoneExists);
        verify(personRepository, times(1)).existsByPhone(person.getPhone());
    }

    @Test
    public void existsByUsername() {
        when(personRepository.existsByUsername(anyString())).thenReturn(true);

        boolean isUsernameExists = personService.existsByUsername(person.getUsername());

        assertTrue(isUsernameExists);
        verify(personRepository, times(1)).existsByUsername(person.getUsername());
    }

    private static void checkPersonFields(Person expected, Person actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getUsername(), actual.getUsername());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getSurName(), actual.getSurName());
        assertEquals(expected.getPhone(), actual.getPhone());
        assertEquals(expected.getRoles(), actual.getRoles());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPasswordConfirm(), actual.getPasswordConfirm());
        assertEquals(expected.getCarts(), actual.getCarts());
        assertEquals(expected.getRegistrationDate(), expected.getRegistrationDate());
    }
}