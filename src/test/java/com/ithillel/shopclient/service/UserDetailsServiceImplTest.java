package com.ithillel.shopclient.service;

import com.ithillel.shopclient.model.Person;

import com.ithillel.shopclient.repository.PersonRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserDetailsServiceImplTest {
    private Person person;
    @Mock
    private PersonRepository personRepository;
    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.person = createPerson();
        this.person.setRoles(new HashSet<>());
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void loadUserByUsernameSuccess() {
        when(personRepository.findByUsername(anyString())).thenReturn(Optional.of(person));

        UserDetails user = userDetailsService.loadUserByUsername(person.getUsername());

        assertNotNull(user);
        assertNotNull(user.getUsername());
        assertNotNull(user.getPassword());
        assertNotNull(user.getAuthorities());
        verify(personRepository, times(1)).findByUsername(person.getUsername());
    }

    @Test
    public void loadUserByUsernameThrowsUsernameNotFoundException() {
        person.setUsername(null);
        Throwable requestException = assertThrows(UsernameNotFoundException.class, () ->
                userDetailsService.loadUserByUsername(person.getUsername()));
        assertEquals(String.format("User '%s' not found", person.getUsername()), requestException.getMessage());
    }
}