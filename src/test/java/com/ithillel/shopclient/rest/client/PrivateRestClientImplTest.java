package com.ithillel.shopclient.rest.client;

import com.ithillel.shopclient.model.CurrencyExchange;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PrivateRestClientImplTest {
    private PrivateRestClientImpl privateRestClient;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.privateRestClient = new PrivateRestClientImpl();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }


    @Test
    public void getJson() {
        CurrencyExchange[] json = privateRestClient.getObjectsFromJson();
        assertNotNull(json);
        assertNotEquals(0, json.length);
    }
}