package com.ithillel.shopclient;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.CurrencyExchange;
import com.ithillel.shopclient.model.Email;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Role;
import com.ithillel.shopclient.model.Shop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public final class ModelsFactory {

    public static Person createPerson() {
        Person person = new Person();
        person.setId(1L);
        person.setUsername("test");
        person.setPassword("test");
        person.setPasswordConfirm("test");
        person.setEmail("admin@gmail.com");
        person.setPhone("+38063444112");
        person.setFirstName("Name");
        person.setSurName("Surname");
        person.setRegistrationDate(new Date());
        person.setCarts(new ArrayList<>());
        person.setRoles(new HashSet<>());
        person.getRoles().add(new Role());

        return person;
    }

    public static PersonDto createPersonDto() {
        PersonDto personDto = new PersonDto();
        personDto.setId(1L);
        personDto.setUsername("test");
        personDto.setPassword("test");
        personDto.setPasswordConfirm("test");
        personDto.setEmail("admin@gmail.com");
        personDto.setPhone("+38063444112");
        personDto.setFirstName("Name");
        personDto.setSurName("Surname");

        return personDto;
    }

    public static Shop createShop() {
        Shop shop = new Shop();
        shop.setId(1L);
        shop.setName("test shop");
        shop.setPhone("+38099765432");
        shop.setDescription("Test description");
        shop.setRating(10.0f);
        shop.setProducts(new ArrayList<>());

        return shop;
    }

    public static ShopDto createShopDto() {
        ShopDto shopDto = new ShopDto();
        shopDto.setId(1L);
        shopDto.setName("test shop");
        shopDto.setPhone("+38099765432");
        shopDto.setDescription("Test description");
        shopDto.setRating(10.0f);
        shopDto.setProducts(new ArrayList<>());

        return shopDto;
    }

    public static Cart createCart(Shop shop, Person person) {
        Cart cartCreate = new Cart();
        cartCreate.setId(1L);
        cartCreate.setShopId(shop);
        cartCreate.setPersonId(person);
        cartCreate.setName("test");
        cartCreate.setProducts(new ArrayList<>());
        cartCreate.setSum(new BigDecimal(0));
        cartCreate.setProducts(new ArrayList<>());

        return cartCreate;
    }

    public static CartDto createCartDto(ShopDto shopDto, PersonDto personDto) {
        CartDto create = new CartDto();
        create.setId(1L);
        create.setShop(shopDto);
        create.setPerson(personDto);
        create.setName("test");
        create.setProducts(new ArrayList<>());
        create.setSum(new BigDecimal(0));
        create.setEuro(new BigDecimal(0));
        create.setUsd(new BigDecimal(0));
        create.setProducts(new ArrayList<>());

        return create;
    }

    public static Product createProduct(Shop shop) {
        Product product = new Product();
        product.setId(1L);
        product.setName("Test product");
        product.setDescription("Test description");
        product.setPrice(new BigDecimal(10));
        product.setShopId(shop);
        product.setDateCreate(new Date());
        shop.getProducts().add(product);

        return product;
    }

    public static ProductDto createProductDto(ShopDto shopDto) {
        ProductDto productDto = new ProductDto();
        productDto.setId(1L);
        productDto.setName("Test product");
        productDto.setDescription("Test description");
        productDto.setPrice(new BigDecimal(10));
        productDto.setShopName(shopDto.getName());
        shopDto.getProducts().add(productDto);

        return productDto;
    }

    public static Role createRole() {
        Role role = new Role();
        role.setId(1L);
        role.setName("Test");
        role.setPersons(new ArrayList<>());

        return role;
    }

    public static Email createEmail() {
        Email email = new Email();
        email.setEmail("test@email.com");
        email.setSubject("test");
        email.setText("test text");

        return email;
    }

    public static CurrencyExchange createCurrencyExchange() {
        CurrencyExchange currencyExchange = new CurrencyExchange();
        currencyExchange.setBaseCcy("USD");
        currencyExchange.setSale(new BigDecimal(1));
        currencyExchange.setBuy(new BigDecimal(1));
        currencyExchange.setCcy("USD");

        return currencyExchange;
    }
}
