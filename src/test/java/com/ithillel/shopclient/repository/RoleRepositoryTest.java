package com.ithillel.shopclient.repository;

import com.ithillel.shopclient.model.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createRole;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RoleRepositoryTest {
    private Role role;
    @Mock
    private RoleRepository roleRepository;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.role = createRole();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(roleRepository.save(any())).thenReturn(role);

        Role saveRole = roleRepository.save(role);

        checkRoleFields(role, saveRole);
        verify(roleRepository, times(1)).save(role);
    }

    @Test
    public void findById() {
        when(roleRepository.findById(anyLong())).thenReturn(Optional.of(role));

        Role findRole = roleRepository.findById(this.role.getId()).orElse(null);

        assert findRole != null;
        checkRoleFields(role, findRole);
        verify(roleRepository, times(1)).findById(role.getId());
    }

    @Test
    public void findAll() {
        List<Role> roles = new ArrayList<>();
        roles.add(role);

        when(roleRepository.findAll()).thenReturn(roles);

        List<Role> rolesRepo = (List<Role>) roleRepository.findAll();

        assertEquals(roles, rolesRepo);
        verify(roleRepository, times(1)).findAll();
    }

    @Test
    public void deleteById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Role " + role.getName() + " successfully deleted");
                    return null;
                }
        ).when(roleRepository).deleteById(anyLong());

        roleRepository.deleteById(role.getId());

        verify(roleRepository, times(1)).deleteById(role.getId());
    }

    private static void checkRoleFields(Role expected, Role actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getPersons(), actual.getPersons());
    }
}