package com.ithillel.shopclient.repository;

import com.ithillel.shopclient.model.Shop;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createShop;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ShopRepositoryTest {
    private Shop shop;
    @Mock
    private ShopRepository shopRepository;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.shop = createShop();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(shopRepository.save(any())).thenReturn(shop);

        Shop save = shopRepository.save(shop);

        checkShopFields(shop, save);
        verify(shopRepository, times(1)).save(shop);
    }

    @Test
    public void findById() {
        when(shopRepository.findById(anyLong())).thenReturn(Optional.of(shop));

        Shop findShop = shopRepository.findById(shop.getId()).orElse(null);

        checkShopFields(shop, findShop);
        verify(shopRepository, times(1)).findById(shop.getId());
    }

    @Test
    public void findAll() {
        List<Shop> shops = new ArrayList<>();
        shops.add(shop);

        when(shopRepository.findAll()).thenReturn(shops);

        List<Shop> shopServiceList = (List<Shop>) shopRepository.findAll();

        assertEquals(shops, shopServiceList);
        verify(shopRepository, times(1)).findAll();
    }

    @Test
    public void deleteById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shop.getName() + " successfully deleted");
                    return null;
                }
        ).when(shopRepository).deleteById(anyLong());

        shopRepository.deleteById(shop.getId());

        verify(shopRepository, times(1)).deleteById(shop.getId());
    }

    @Test
    public void findByName() {
        when(shopRepository.findByName(anyString())).thenReturn(Optional.of(shop));

        Shop byName = shopRepository.findByName(shop.getName()).orElse(new Shop());

        checkShopFields(shop, byName);
        verify(shopRepository, times(1)).findByName(anyString());
    }

    private static void checkShopFields(Shop expected, Shop actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getProducts(), actual.getProducts());
        assertEquals(expected.getRating(), actual.getRating());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getPhone(), actual.getPhone());
    }
}