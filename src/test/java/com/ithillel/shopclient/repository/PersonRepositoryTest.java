package com.ithillel.shopclient.repository;

import com.ithillel.shopclient.model.Person;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PersonRepositoryTest {
    private Person person;
    @Mock
    private PersonRepository personRepository;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.person = createPerson();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(personRepository.save(any())).thenReturn(person);

        Person savePerson = personRepository.save(person);

        checkPersonFields(person, savePerson);
        verify(personRepository, times(1)).save(person);
    }

    @Test
    public void findPersonById() {
        when(personRepository.findById(anyLong())).thenReturn(Optional.of(person));

        Person getPerson = personRepository.findById(person.getId()).orElse(null);

        checkPersonFields(person, getPerson);
        verify(personRepository, times(1)).findById(person.getId());
    }

    @Test
    public void findByEmail() {
        when(personRepository.findByEmail(anyString())).thenReturn(Optional.of(person));

        Person personByEmail = personRepository.findByEmail(person.getEmail()).orElse(null);

        checkPersonFields(person, personByEmail);
        verify(personRepository, times(1)).findByEmail(person.getEmail());
    }

    @Test
    public void findByUsername() {
        when(personRepository.findByUsername(anyString())).thenReturn(Optional.of(person));

        Person personByUsername = personRepository.findByUsername(person.getUsername()).orElse(null);

        checkPersonFields(person, personByUsername);
        verify(personRepository, times(1)).findByUsername(person.getUsername());
    }

    @Test
    public void getAllPersons() {
        List<Person> list = new ArrayList<>();
        list.add(person);

        when(personRepository.findAll()).thenReturn(list);

        List<Person> serviceList = (List<Person>) personRepository.findAll();

        assertEquals(list, serviceList);
        verify(personRepository, times(1)).findAll();
    }

    @Test
    public void deleteById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Person " + person.getUsername() + " successfully deleted");
                    return null;
                }
        ).when(personRepository).deleteById(anyLong());

        personRepository.deleteById(person.getId());

        verify(personRepository, times(1)).deleteById(person.getId());
    }

    @Test
    public void existsByEmail() {
        when(personRepository.existsByEmail(anyString())).thenReturn(true);

        boolean isMailExists = personRepository.existsByEmail(person.getEmail());

        assertTrue(isMailExists);
        verify(personRepository, times(1)).existsByEmail(person.getEmail());
    }

    @Test
    public void existsByPhone() {
        when(personRepository.existsByPhone(anyString())).thenReturn(true);

        boolean isPhoneExists = personRepository.existsByPhone(person.getPhone());

        assertTrue(isPhoneExists);
        verify(personRepository, times(1)).existsByPhone(person.getPhone());
    }

    @Test
    public void existsByUsername() {
        when(personRepository.existsByUsername(anyString())).thenReturn(true);

        boolean isUsernameExists = personRepository.existsByUsername(person.getUsername());

        assertTrue(isUsernameExists);
        verify(personRepository, times(1)).existsByUsername(person.getUsername());
    }

    private static void checkPersonFields(Person expected, Person actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getUsername(), actual.getUsername());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getSurName(), actual.getSurName());
        assertEquals(expected.getPhone(), actual.getPhone());
        assertEquals(expected.getRoles(), actual.getRoles());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPasswordConfirm(), actual.getPasswordConfirm());
        assertEquals(expected.getCarts(), actual.getCarts());
        assertEquals(expected.getRegistrationDate(), expected.getRegistrationDate());
    }
}