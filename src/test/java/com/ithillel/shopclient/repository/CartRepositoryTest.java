package com.ithillel.shopclient.repository;

import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createCart;
import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CartRepositoryTest {

    private Cart cart;
    @Mock
    private CartRepository cartRepository;
    private MockitoSession session;

    @Before
    public void setUp() {
        Person person = createPerson();
        Shop shop = createShop();
        Product product = createProduct(shop);
        this.cart = createCart(shop, person);
        this.cart.getProducts().add(product);
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void findAll() {
        List<Cart> list = new ArrayList<>();
        list.add(cart);

        when(cartRepository.findAll()).thenReturn(list);

        List<Cart> serviceList = (List<Cart>) cartRepository.findAll();

        assertEquals(list, serviceList);
        verify(cartRepository, times(1)).findAll();
    }

    @Test
    public void save() {
        when(cartRepository.save(any())).thenReturn(cart);

        Cart saveCart = cartRepository.save(cart);

        checkCartFields(cart, saveCart);
        verify(cartRepository, times(1)).save(cart);
    }

    @Test
    public void findById() {
        when(cartRepository.findById(anyLong())).thenReturn(Optional.ofNullable(cart));

        Cart getCart = cartRepository.findById(1L).orElse(null);

        checkCartFields(cart, getCart);
        verify(cartRepository, times(1)).findById(1L);
    }

    @Test
    public void update() {
        when(cartRepository.save(any())).thenReturn(cart);

        Cart update = cartRepository.save(cart);

        checkCartFields(cart, update);
        verify(cartRepository, times(1)).save(cart);
    }

    @Test
    public void deleteById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Cart " + cart.getName() + " successfully deleted");
                    return null;
                }
        ).when(cartRepository).deleteById(anyLong());
        cartRepository.deleteById(cart.getId());
        verify(cartRepository, times(1)).deleteById(cart.getId());
    }

    private static void checkCartFields(Cart expected, Cart actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getSum(), actual.getSum());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getShopId(), actual.getShopId());
        assertEquals(expected.getPersonId(), actual.getPersonId());
        assertEquals(expected.getProducts(), actual.getProducts());
        assertEquals(expected.getOrderDate(), actual.getOrderDate());
    }
}