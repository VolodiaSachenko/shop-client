package com.ithillel.shopclient.repository;

import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductRepositoryTest {
    private Product product;
    @Mock
    private ProductRepository productRepository;
    private MockitoSession session;

    @Before
    public void setUp() {
        Shop shop = createShop();
        this.product = createProduct(shop);
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(productRepository.save(any())).thenReturn(product);

        Product saveProduct = productRepository.save(product);

        checkProductFields(product, saveProduct);
        verify(productRepository, times(1)).save(product);
    }

    @Test
    public void findById() {
        when(productRepository.findById(anyLong())).thenReturn(Optional.of(product));

        Product findProduct = productRepository.findById(anyLong()).orElse(null);

        checkProductFields(product, findProduct);
        verify(productRepository, times(1)).findById(anyLong());
    }

    @Test
    public void findAll() {
        List<Product> products = new ArrayList<>();
        products.add(product);

        when(productRepository.findAll()).thenReturn(products);

        List<Product> serviceList = (List<Product>) productRepository.findAll();

        assertEquals(products, serviceList);
        verify(productRepository, times(1)).findAll();
    }

    @Test
    public void deleteById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product " + product.getName() + " successfully deleted");
                    return null;
                }
        ).when(productRepository).deleteById(anyLong());

        productRepository.deleteById(product.getId());

        verify(productRepository, times(1)).deleteById(product.getId());
    }

    @Test
    public void findByShopId() {
        when(productRepository.findByShopId(any())).thenReturn(Optional.of(new ArrayList<>()));

        List<Product> findProducts = productRepository.findByShopId(any()).orElse(null);

        assertNotNull(findProducts);
        verify(productRepository, times(1)).findByShopId(any());
    }

    private static void checkProductFields(Product expected, Product actual) {
        assertEquals(expected, actual);
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getShopId(), actual.getShopId());
        assertEquals(expected.getPrice(), actual.getPrice());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getDateCreate(), actual.getDateCreate());
    }
}