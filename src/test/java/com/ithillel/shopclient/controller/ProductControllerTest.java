package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;

import com.ithillel.shopclient.facade.ProductFacade;
import com.ithillel.shopclient.facade.ProductFacadeImpl;
import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.ProductService;
import com.ithillel.shopclient.service.ShopService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

import static com.ithillel.shopclient.ModelsFactory.createProductDto;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductControllerTest {
    private ProductDto productDto;
    @Mock
    private ProductFacade productFacade;
    @InjectMocks
    private ProductController productController;
    private MockMvc mockMvc;
    private MockitoSession session;

    @Before
    public void setUp() {
        ShopDto shopDto = createShopDto();
        this.productDto = createProductDto(shopDto);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(
                        new ProductController
                                (new ProductFacadeImpl(
                                        mock(ProductService.class),
                                        mock(ShopService.class),
                                        mock(CurrencyExchangeService.class))))
                .build();

        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void addProductForm() throws Exception {
        mockProductControllerForMockMvc();

        String content = productController.addProductForm(mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/product/add", content);
    }

    @Test
    public void createProduct() throws Exception {
        when(productFacade.save(any())).thenReturn(productDto);
        ModelAndView modelAndView = productController.createProduct(productDto);

        assertNotNull(modelAndView);
        mockMvcPerformTestsWithRedirect("/product/add/create");
        verify(productFacade, times(1)).save(productDto);
    }

    @Test
    public void getAllProducts() throws Exception {
        when(productFacade.getAllProducts()).thenReturn(new ArrayList<>());

        String content = productController.getAllProducts(mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/product", content);
        verify(productFacade, times(1)).getAllProducts();
    }

    @Test
    public void showUpdateForm() throws Exception {
        mockProductControllerForMockMvc();
        when(productFacade.findProductById(anyString())).thenReturn(productDto);

        String result = productController.showUpdateForm("1", mock(Model.class));

        assertNotNull(result);
        mockMvcPerformTests("/product/update/id1", result);
        verify(productFacade, times(1)).findProductById("1");
    }

    @Test
    public void updateProduct() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product " + productDto.getName() + " successfully updated.");
                    return null;
                }
        ).when(productFacade).updateProduct(any());

        ModelAndView modelAndView = productController.updateProduct(productDto);

        assertNotNull(modelAndView);
        mockMvcPerformTestsWithRedirect("/product/update/confirm");
        verify(productFacade, times(1)).updateProduct(productDto);
    }

    @Test
    public void deleteProduct() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product " + productDto.getName() + " successfully deleted.");
                    return null;
                }
        ).when(productFacade).deleteProductById(anyString());

        ModelAndView modelAndView = productController.deleteProduct("1");

        assertNotNull(modelAndView);
        mockMvc.perform(get("/product/delete/id1"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/product"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(productFacade, times(1)).deleteProductById("1");
    }

    private void mockProductControllerForMockMvc() {
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(mock(ProductController.class))
                .build();
    }

    private void mockMvcPerformTests(String url, String content) throws Exception {
        mockMvc.perform(get(url).content(content))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    private void mockMvcPerformTestsWithRedirect(String url) throws Exception {
        mockMvc.perform(post(url))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/product"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}