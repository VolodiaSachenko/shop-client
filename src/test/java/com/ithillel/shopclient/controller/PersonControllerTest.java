package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.facade.PersonFacade;
import com.ithillel.shopclient.facade.PersonFacadeImpl;

import com.ithillel.shopclient.service.EmailService;
import com.ithillel.shopclient.service.PersonService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;

import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PersonControllerTest {
    private PersonDto personDto;
    @Mock
    private PersonFacade personFacade;
    @InjectMocks
    private PersonController personController;
    private MockMvc mockMvc;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.personDto = createPersonDto();
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(
                        new PersonController
                                (new PersonFacadeImpl(
                                        mock(PersonService.class),
                                        mock(EmailService.class))))
                .build();

        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void login() {
        String content = personController.login(mock(Model.class));

        assertNotNull(content);
    }

    @Test
    public void confirmLogin() throws Exception {
        mockMvc.perform(post("/login/confirm"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void register() {
        String content = personController.register(mock(Model.class));

        assertNotNull(content);
    }

    @Test
    public void registerUser() throws Exception {
        mockPersonControllerForMockMvc();
        when(personFacade.save(any())).thenReturn(personDto);

        String content = personController.registerUser(personDto, mock(BindingResult.class), mock(Model.class));

        assertNotNull(content);
        mockMvc.perform(post("/registration/confirm").content(content))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(personFacade, times(1)).save(personDto);
    }

    @Test
    public void allPersons() throws Exception {
        when(personFacade.getAllPersons(any(), any())).thenReturn(new ArrayList<>());
        String content = personController.allPersons(
                mock(Model.class), mock(Principal.class), mock(HttpServletRequest.class));

        assertNotNull(content);
        mockMvcPerformTests("/person", content);
        verify(personFacade, times(1)).getAllPersons(any(), any());
    }

    @Test
    public void showUpdateForm() throws Exception {
        mockPersonControllerForMockMvc();
        when(personFacade.findPersonById(anyString())).thenReturn(personDto);

        String content = personController.showUpdateForm("1", mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/person/update/id1", content);
        verify(personFacade, times(1)).findPersonById("1");
    }

    @Test
    public void editPerson() {
        String content = personController.editPerson(mock(Model.class), mock(Principal.class));

        assertNotNull(content);
    }

    @Test
    public void updatePerson() throws Exception {
        mockPersonControllerForMockMvc();
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Person " + personDto.getUsername() + " successfully updated.");
                    return null;
                }
        ).when(personFacade).updatePerson(any());

        ModelAndView modelAndView = personController.updatePerson(personDto, mock(BindingResult.class));

        assertNotNull(modelAndView);
        mockMvc.perform(post("/person/update/confirm"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(personFacade, times(1)).updatePerson(personDto);
    }

    @Test
    public void deletePerson() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Person " + personDto.getUsername() + " successfully deleted.");
                    return null;
                }
        ).when(personFacade).deletePersonById(anyString());

        ModelAndView modelAndView = personController.deletePerson("1");

        assertNotNull(modelAndView);
        mockMvc.perform(get("/person/delete/id1"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/person"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(personFacade, times(1)).deletePersonById("1");
    }

    private void mockPersonControllerForMockMvc() {
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(mock(PersonController.class))
                .build();
    }

    private void mockMvcPerformTests(String url, String content) throws Exception {
        mockMvc.perform(get(url).content(content))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}