package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.facade.ShopFacade;

import com.ithillel.shopclient.facade.ShopFacadeImpl;
import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.PersonService;
import com.ithillel.shopclient.service.ProductService;
import com.ithillel.shopclient.service.ShopService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.ArrayList;

import static com.ithillel.shopclient.ModelsFactory.createCartDto;
import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ShopControllerTest {
    private ShopDto shopDto;
    @Mock
    private ShopFacade shopFacade;
    @InjectMocks
    private ShopController shopController;
    private MockMvc mockMvc;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.shopDto = createShopDto();
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(
                        new ShopController
                                (new ShopFacadeImpl(
                                        mock(ShopService.class),
                                        mock(ProductService.class),
                                        mock(PersonService.class),
                                        mock(CurrencyExchangeService.class))))
                .build();

        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void addShopForm() throws Exception {
        mockShopControllerForMockMvc();

        String content = shopController.addShopForm(mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/shop/add", content);
    }

    @Test
    public void createShop() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shopDto.getName() + " successfully created.");
                    return null;
                }
        ).when(shopFacade).createShop(any());

        ModelAndView modelAndView = shopController.createShop(shopDto);

        assertNotNull(modelAndView);
        mockMvcPerformTestsWithRedirect("/shop/add/create");
        verify(shopFacade, times(1)).createShop(shopDto);
    }

    @Test
    public void getAllShops() throws Exception {
        when(shopFacade.getAllShops()).thenReturn(new ArrayList<>());

        String content = shopController.getAllShops(mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/shop", content);
        verify(shopFacade, times(1)).getAllShops();
    }

    @Test
    public void showUpdateForm() throws Exception {
        mockShopControllerForMockMvc();
        when(shopFacade.findShopById(anyString())).thenReturn(shopDto);

        String result = shopController.showUpdateForm("1", mock(Model.class));

        assertNotNull(result);
        mockMvcPerformTests("/shop/update/id1", result);
        verify(shopFacade, times(1)).findShopById("1");
    }

    @Test
    public void updateShop() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shopDto.getName() + " successfully updated.");
                    return null;
                }
        ).when(shopFacade).updateShop(any());

        ModelAndView modelAndView = shopController.updateShop(shopDto);

        assertNotNull(modelAndView);
        mockMvcPerformTestsWithRedirect("/shop/update/confirm");
        verify(shopFacade, times(1)).updateShop(shopDto);
    }

    @Test
    public void deleteShop() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shopDto.getName() + " successfully deleted.");
                    return null;
                }
        ).when(shopFacade).deleteShopById(anyString());

        ModelAndView modelAndView = shopController.deleteShop("1");

        assertNotNull(modelAndView);
        mockMvc.perform(get("/shop/delete/id1"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/shop"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(shopFacade, times(1)).deleteShopById("1");
    }

    @Test
    public void getAllShopProducts() throws Exception {
        mockShopControllerForMockMvc();
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop products test");
                    return null;
                }
        ).when(shopFacade).getAllProducts(anyString(), any());

        String content = shopController.getAllShopProducts("1", mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/shop/id1/products", content);
        verify(shopFacade, times(1)).getAllProducts(anyString(), any());
    }

    @Test
    public void addToCartForm() throws Exception {
        mockShopControllerForMockMvc();
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Add to cart form");
                    return null;
                }
        ).when(shopFacade).addToCartForm(anyString(), anyString(), any(), any());

        String content = shopController.addToCartForm("1", "1", mock(Model.class), mock(Principal.class));

        assertNotNull(content);
        mockMvcPerformTests("/shop/id1/add-to-cart/product-id1", content);
        verify(shopFacade, times(1))
                .addToCartForm(anyString(), anyString(), any(), any());
    }

    @Test
    public void addToCart() throws Exception {
        mockShopControllerForMockMvc();
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Add to cart method test");
                    return null;
                }
        ).when(shopFacade).addProductToCart(any());
        PersonDto personDto = createPersonDto();
        CartDto cartDto = createCartDto(shopDto, personDto);

        ModelAndView modelAndView = shopController.addToCart(cartDto);

        assertNotNull(modelAndView);

        mockMvc.perform(post("/confirm-order"))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(shopFacade, times(1)).addProductToCart(cartDto);
    }

    private void mockShopControllerForMockMvc() {
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(mock(ShopController.class))
                .build();
    }

    private void mockMvcPerformTests(String url, String content) throws Exception {
        mockMvc.perform(get(url).content(content))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    private void mockMvcPerformTestsWithRedirect(String url) throws Exception {
        mockMvc.perform(post(url))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/shop"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}