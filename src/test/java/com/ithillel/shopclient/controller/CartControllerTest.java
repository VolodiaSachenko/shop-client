package com.ithillel.shopclient.controller;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.facade.CartFacade;
import com.ithillel.shopclient.facade.CartFacadeImpl;

import com.ithillel.shopclient.service.CartService;
import com.ithillel.shopclient.service.PersonService;
import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.ShopService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;

import static com.ithillel.shopclient.ModelsFactory.createCartDto;
import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static com.ithillel.shopclient.ModelsFactory.createProductDto;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CartControllerTest {
    private CartDto cartDto;
    @Mock
    private CartFacade cartFacade;
    @InjectMocks
    private CartController cartController;
    private MockMvc mockMvc;
    private MockitoSession session;

    @Before
    public void setUp() {
        PersonDto personDto = createPersonDto();
        ShopDto shopDto = createShopDto();
        ProductDto productDto = createProductDto(shopDto);
        this.cartDto = createCartDto(shopDto, personDto);
        this.cartDto.getProducts().add(productDto);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(
                        new CartController
                                (new CartFacadeImpl(
                                        mock(CartService.class),
                                        mock(PersonService.class),
                                        mock(ShopService.class),
                                        mock(CurrencyExchangeService.class))))
                .build();

        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void addCartForm() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Add cart form tests");
                    return null;
                }
        ).when(cartFacade).addCartForm(any());

        String content = cartController.addCartForm(mock(Model.class));

        assertNotNull(content);
        mockMvcPerformTests("/cart/add", content);
        verify(cartFacade, times(1)).addCartForm(any());
    }

    @Test
    public void createCart() throws Exception {
        when(cartFacade.save(any())).thenReturn(cartDto);

        ModelAndView modelAndView = cartController.createCart(cartDto);

        assertNotNull(modelAndView);
        mockMvcPerformTestsWithRedirect("/cart/add/create");
        verify(cartFacade, times(1)).save(cartDto);
    }

    @Test
    public void getAllCarts() throws Exception {
        when(cartFacade.getAll(any(), any())).thenReturn(new ArrayList<>());

        String result = cartController.getAllCarts(
                mock(Model.class), mock(Principal.class), mock(HttpServletRequest.class));

        assertNotNull(result);
        mockMvcPerformTests("/cart", result);
        verify(cartFacade, times(1)).getAll(any(), any());
    }

    @Test
    public void getAllProducts() throws Exception {
        mockCartControllerForMockMvc();
        when(cartFacade.findById(anyString())).thenReturn(cartDto);

        String result = cartController.getAllProducts("1", mock(Model.class));

        assertNotNull(result);
        mockMvcPerformTests("/cart/id1/products", result);
        verify(cartFacade, times(1)).findById("1");
    }

    @Test
    public void showUpdateForm() throws Exception {
        mockCartControllerForMockMvc();
        when(cartFacade.findById(anyString())).thenReturn(cartDto);

        String result = cartController.showUpdateForm("1", mock(Model.class));

        assertNotNull(result);
        mockMvcPerformTests("/cart/update/id1", result);
        verify(cartFacade, times(1)).findById("1");
    }

    @Test
    public void updateCart() throws Exception {
        when(cartFacade.update(any())).thenReturn(cartDto);

        ModelAndView modelAndView = cartController.updateCart(cartDto);

        assertNotNull(modelAndView);
        mockMvcPerformTestsWithRedirect("/cart/update/confirm");
        verify(cartFacade, times(1)).update(cartDto);
    }

    @Test
    public void deleteCart() throws Exception {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Cart " + cartDto.getName() + " successfully deleted.");
                    return null;
                }
        ).when(cartFacade).deleteById(anyString());

        ModelAndView modelAndView = cartController.deleteCart("1");

        assertNotNull(modelAndView);
        mockMvc.perform(get("/cart/delete/id1"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/cart"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(cartFacade, times(1)).deleteById("1");
    }

    @Test
    public void deleteProduct() throws Exception {
        mockCartControllerForMockMvc();
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product " + cartDto.getName() + " successfully deleted.");
                    return null;
                }
        ).when(cartFacade).deleteProduct(anyString(), anyString());

        ModelAndView modelAndView = cartController.deleteProduct("1", "1");

        assertNotNull(modelAndView);
        mockMvc.perform(post("/cart/delete/id1/1"))
                .andExpect(status().is(200))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        verify(cartFacade, times(1)).deleteProduct(anyString(), anyString());
    }

    private void mockCartControllerForMockMvc() {
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(mock(CartController.class))
                .build();
    }

    private void mockMvcPerformTests(String url, String content) throws Exception {
        mockMvc.perform(get(url).content(content))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    private void mockMvcPerformTestsWithRedirect(String url) throws Exception {
        mockMvc.perform(post(url))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/cart"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}