package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.ModelsFactory;
import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.dto.ShopDto;

import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.PersonService;
import com.ithillel.shopclient.service.ProductService;
import com.ithillel.shopclient.service.ShopService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static com.ithillel.shopclient.ModelsFactory.createCartDto;
import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createProductDto;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ShopFacadeImplTest {
    private Shop shop;
    private ShopDto shopDto;
    @Mock
    private ShopService shopService;
    @Mock
    private ProductService productService;
    @Mock
    private PersonService personService;
    @Mock
    private CurrencyExchangeService currencyExchangeService;
    @InjectMocks
    private ShopFacadeImpl shopFacade;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.shop = ModelsFactory.createShop();
        this.shopDto = createShopDto();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void createShop() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shop.getName() + " successfully created");
                    return null;
                }
        ).when(shopService).createShop(any());
        shopFacade.createShop(shopDto);
        verify(shopService, times(1)).createShop(shop);
    }

    @Test
    public void findShopById() {
        when(shopService.findShopById(anyLong())).thenReturn(shop);

        ShopDto findDto = shopFacade.findShopById("1");

        checkShopDtoFields(shopDto, findDto);
        verify(shopService, times(1)).findShopById(1L);
    }

    @Test
    public void getAllShops() {
        List<Shop> shops = new ArrayList<>();
        shops.add(shop);

        when(shopService.getAllShops()).thenReturn(shops);

        List<ShopDto> facadeList = shopFacade.getAllShops();

        assertEquals(shops.size(), facadeList.size());
        verify(shopService, times(1)).getAllShops();
    }

    @Test
    public void getAllProducts() {
        List<Product> products = new ArrayList<>();
        products.add(createProduct(shop));

        when(shopService.findShopById(anyLong())).thenReturn(shop);
        when(shopService.getAllProducts(any())).thenReturn(products);
        when(currencyExchangeService.exchangeToEuros(any())).thenReturn(new BigDecimal(0));
        when(currencyExchangeService.exchangeToDollars(any())).thenReturn(new BigDecimal(0));

        shopFacade.getAllProducts(shop.getId().toString(), mock(Model.class));

        verify(shopService, times(1))
                .getAllProducts(shop);
    }

    @Test
    public void addToCartForm() {
        Person person = createPerson();
        List<Person> persons = new ArrayList<>();
        persons.add(person);

        when(shopService.findShopById(anyLong())).thenReturn(shop);
        when(productService.findProductById(anyLong())).thenReturn(createProduct(shop));
        when(personService.getAllPersons()).thenReturn(persons);

        shopFacade.addToCartForm("1", "1", mock(Model.class), mock(Principal.class));

        verify(personService, times(1)).getAllPersons();
    }

    @Test
    public void updateShop() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shop.getName() + " successfully updated");
                    return null;
                }
        ).when(shopService).updateShop(any());
        shopFacade.updateShop(shopDto);
        verify(shopService, times(1)).updateShop(any());
    }

    @Test
    public void deleteShopById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shop.getName() + " successfully deleted");
                    return null;
                }
        ).when(shopService).deleteShopById(anyLong());
        shopFacade.deleteShopById(shop.getId().toString());
        verify(shopService, times(1)).deleteShopById(shop.getId());
    }

    @Test
    public void addProductToCart() {
        when(personService.findByEmail(anyString())).thenReturn(createPerson());
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Shop " + shop.getName() + " successfully deleted");
                    return null;
                }
        ).when(shopService).addProductToCart(anyLong(), anyLong(), anyLong());
        PersonDto personDto = createPersonDto();
        CartDto cartDto = createCartDto(shopDto, personDto);
        cartDto.getProducts().add(createProductDto(shopDto));
        cartDto.setCartProduct(createProductDto(shopDto));

        shopFacade.addProductToCart(cartDto);
        verify(shopService, times(1))
                .addProductToCart(1L, 1L, 1L);
    }

    private static void checkShopDtoFields(ShopDto expected, ShopDto actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getPhone(), actual.getPhone());
        assertEquals(expected.getRating(), actual.getRating());
        assertEquals(expected.getProducts(), actual.getProducts());
    }
}