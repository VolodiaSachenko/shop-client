package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;

import com.ithillel.shopclient.service.CurrencyExchangeService;
import com.ithillel.shopclient.service.ProductService;
import com.ithillel.shopclient.service.ShopService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createProductDto;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductFacadeImplTest {
    private Product product;
    private ProductDto productDto;
    @Mock
    private ProductService productService;
    @Mock
    private ShopService shopService;
    @Mock
    private CurrencyExchangeService currencyExchangeService;
    @InjectMocks
    private ProductFacadeImpl productFacade;
    private MockitoSession session;

    @Before
    public void setUp() {
        Shop shop = createShop();
        this.product = createProduct(shop);
        ShopDto shopDto = createShopDto();
        this.productDto = createProductDto(shopDto);
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(productService.save(any())).thenReturn(product);

        ProductDto saveDto = productFacade.save(productDto);

        checkProductDtoFields(productDto, saveDto);
        verify(productService, times(1)).save(product);
    }

    @Test
    public void findProductById() {
        when(productService.findProductById(anyLong())).thenReturn(product);

        ProductDto findDto = productFacade.findProductById("1");

        checkProductDtoFields(productDto, findDto);
        verify(productService, times(1)).findProductById(any());
    }

    @Test
    public void getAllProducts() {
        List<Product> productList = new ArrayList<>();
        productList.add(product);
        when(productService.getAllProducts()).thenReturn(productList);
        when(currencyExchangeService.exchangeToEuros(any())).thenReturn(new BigDecimal(0));
        when(currencyExchangeService.exchangeToDollars(any())).thenReturn(new BigDecimal(0));

        List<ProductDto> productDtoList = productFacade.getAllProducts();

        assertEquals(productList.size(), productDtoList.size());
        verify(productService, times(1)).getAllProducts();
    }

    @Test
    public void updateProduct() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product " + product.getName() + " successfully updated");
                    return null;
                }
        ).when(productService).updateProduct(any());
        productFacade.updateProduct(productDto);
        verify(productService, times(1)).updateProduct(any());
    }

    @Test
    public void deleteProductById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product " + product.getName() + " successfully deleted");
                    return null;
                }
        ).when(productService).deleteProductById(anyLong());
        productFacade.deleteProductById("1");
        verify(productService, times(1)).deleteProductById(1L);
    }

    @Test
    public void addProductForm() {
        List<Shop> shopList = new ArrayList<>();
        shopList.add(createShop());
        when(shopService.getAllShops()).thenReturn(shopList);

        List<ShopDto> shopDtoList = productFacade.addProductForm();

        assertEquals(shopList.size(), shopDtoList.size());
        verify(shopService, times(1)).getAllShops();
    }

    private static void checkProductDtoFields(ProductDto expected, ProductDto actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getShopName(), actual.getShopName());
        assertEquals(expected.getPrice(), actual.getPrice());
    }
}