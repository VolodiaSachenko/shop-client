package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.PersonDto;

import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.service.PersonService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;

import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PersonFacadeImplTest {
    private Person person;
    private PersonDto personDto;
    @Mock
    private PersonService personService;
    @InjectMocks
    private PersonFacadeImpl personFacade;
    private MockitoSession session;

    @Before
    public void setUp() {
        this.person = createPerson();
        this.personDto = createPersonDto();
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() throws MessagingException {
        when(personService.existsByUsername(anyString())).thenReturn(false);
        when(personService.save(any())).thenReturn(person);

        PersonDto saveDto = personFacade.save(personDto);

        checkPersonDtoFields(personDto, saveDto);
        verify(personService, times(1)).save(person);
    }

    @Test
    public void findPersonById() {
        when(personService.findPersonById(anyLong())).thenReturn(person);

        PersonDto findDto = personFacade.findPersonById("1");

        checkPersonDtoFields(personDto, findDto);
        verify(personService, times(1)).findPersonById(any());
    }

    @Test
    public void findByUsername() {
        when(personService.findByUsername(anyString())).thenReturn(person);

        PersonDto findByName = personFacade.findByUsername(person.getUsername());

        checkPersonDtoFields(personDto, findByName);
        verify(personService, times(1)).findByUsername(anyString());
    }

    @Test
    public void getAllPersons() {
        when(personService.getAllPersons()).thenReturn(new ArrayList<>());

        personFacade.getAllPersons(mock(Principal.class), mock(HttpServletRequest.class));

        verify(personService, times(1)).getAllPersons();
    }

    @Test
    public void updatePerson() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Person " + person.getUsername() + " successfully updated");
                    return null;
                }
        ).when(personService).updatePerson(any());
        personFacade.updatePerson(personDto);
        verify(personService, times(1)).updatePerson(any());
    }

    @Test
    public void deletePersonById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Person " + person.getUsername() + " successfully deleted");
                    return null;
                }
        ).when(personService).deletePersonById(anyLong());
        personFacade.deletePersonById("1");
        verify(personService, times(1)).deletePersonById(1L);
    }

    private static void checkPersonDtoFields(PersonDto expected, PersonDto actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getSurName(), actual.getSurName());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getPhone(), actual.getPhone());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getUsername(), actual.getUsername());
    }
}