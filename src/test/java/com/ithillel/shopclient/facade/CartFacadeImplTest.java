package com.ithillel.shopclient.facade;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.model.Cart;

import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.service.CartService;
import com.ithillel.shopclient.service.PersonService;
import com.ithillel.shopclient.service.ShopService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;

import static com.ithillel.shopclient.ModelsFactory.createCart;
import static com.ithillel.shopclient.ModelsFactory.createCartDto;
import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createProductDto;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CartFacadeImplTest {
    private CartDto cartDto;
    private Cart cart;
    @Mock
    private CartService cartService;
    @Mock
    private ShopService shopService;
    @Mock
    private PersonService personService;
    @InjectMocks
    private CartFacadeImpl cartFacade;
    private MockitoSession session;

    @Before
    public void setUp() {
        Person person = createPerson();
        Shop shop = createShop();
        Product product = createProduct(shop);
        this.cart = createCart(shop, person);
        this.cart.getProducts().add(product);

        PersonDto personDto = createPersonDto();
        ShopDto shopDto = createShopDto();
        ProductDto productDto = createProductDto(shopDto);
        this.cartDto = createCartDto(shopDto, personDto);
        this.cartDto.getProducts().add(productDto);
        this.session = Mockito.mockitoSession()
                .initMocks(this)
                .startMocking();
    }

    @After
    public void afterMethod() {
        session.finishMocking();
    }

    @Test
    public void save() {
        when(cartService.save(any())).thenReturn(cart);

        CartDto saveDto = cartFacade.save(cartDto);

        checkCartDtoFields(cartDto, saveDto);
        verify(cartService, times(1)).save(any());
    }

    @Test
    public void findById() {
        when(cartService.findById(anyLong())).thenReturn(cart);

        CartDto findDto = cartFacade.findById("1");

        checkCartDtoFields(cartDto, findDto);
        verify(cartService, times(1)).findById(any());
    }

    @Test
    public void getAll() {
        when(cartService.getAll()).thenReturn(new ArrayList<>());

        cartFacade.getAll(mock(Principal.class), mock(HttpServletRequest.class));

        verify(cartService, times(1)).getAll();
    }

    @Test
    public void update() {
        when(cartService.update(any())).thenReturn(cart);

        CartDto updateDto = cartFacade.update(cartDto);

        checkCartDtoFields(cartDto, updateDto);
        verify(cartService, times(1)).update(any());
    }

    @Test
    public void deleteById() {
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Cart " + cart.getName() + " successfully deleted");
                    return null;
                }
        ).when(cartService).deleteById(anyLong());
        cartFacade.deleteById(cart.getId().toString());
        verify(cartService, times(1)).deleteById(cart.getId());
    }

    @Test
    public void addCartForm() {
        when(personService.getAllPersons()).thenReturn(new ArrayList<>());
        when(shopService.getAllShops()).thenReturn(new ArrayList<>());

        cartFacade.addCartForm(mock(Model.class));

        verify(personService, times(1)).getAllPersons();
        verify(shopService, times(1)).getAllShops();
    }

    @Test
    public void deleteProduct() {
        when(cartService.findById(anyLong())).thenReturn(cart);
        doAnswer(invocationOnMock ->
                {
                    System.out.println("Product successfully deleted");
                    return null;
                }
        ).when(shopService).deleteProductFromCart(anyLong(), anyLong(), anyLong());
        cartFacade.deleteProduct("1", "1");
        verify(shopService, times(1))
                .deleteProductFromCart(1L, 1L, 1L);
    }

    private static void checkCartDtoFields(CartDto expected, CartDto actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getSum(), actual.getSum());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getShop(), actual.getShop());
        assertEquals(expected.getPerson(), actual.getPerson());
        assertEquals(expected.getProducts(), actual.getProducts());
    }
}