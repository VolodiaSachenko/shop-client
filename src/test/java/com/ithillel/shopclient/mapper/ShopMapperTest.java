package com.ithillel.shopclient.mapper;

import com.ithillel.shopclient.dto.ShopDto;

import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.utils.mapper.ShopMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.ithillel.shopclient.ModelsFactory.createShop;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ShopMapperTest {
    private Shop shop;
    private ShopDto shopDto;

    @Before
    public void setUp() {
        this.shop = createShop();
        this.shopDto = createShopDto();
    }

    @Test
    public void fromDto() {
        Shop shop = ShopMapper.fromDto(shopDto);

        assertEquals(shopDto.getId(), shop.getId());
        assertEquals(shopDto.getName(), shop.getName());
        assertEquals(shopDto.getDescription(), shop.getDescription());
        assertEquals(shopDto.getPhone(), shop.getPhone());
        assertEquals(shopDto.getRating(), shop.getRating());
        assertEquals(shopDto.getProducts().size(), shop.getProducts().size());
    }

    @Test
    public void toDto() {
        ShopDto shopDto = ShopMapper.toDto(shop);

        assertEquals(shop.getId(), shopDto.getId());
        assertEquals(shop.getName(), shopDto.getName());
        assertEquals(shop.getDescription(), shopDto.getDescription());
        assertEquals(shop.getPhone(), shopDto.getPhone());
        assertEquals(shop.getRating(), shopDto.getRating());
        assertEquals(shop.getProducts().size(), shopDto.getProducts().size());
    }
}