package com.ithillel.shopclient.mapper;

import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.model.Person;

import com.ithillel.shopclient.utils.mapper.PersonMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PersonMapperTest {
    private Person person;
    private PersonDto personDto;

    @Before
    public void setUp() {
        this.personDto = createPersonDto();
        this.person = createPerson();
    }

    @Test
    public void fromDto() {
        Person person = PersonMapper.fromDto(personDto);

        assertEquals(personDto.getId(), person.getId());
        assertEquals(personDto.getSurName(), person.getSurName());
        assertEquals(personDto.getFirstName(), person.getFirstName());
        assertEquals(personDto.getPhone(), person.getPhone());
        assertEquals(personDto.getEmail(), person.getEmail());
        assertEquals(personDto.getUsername(), person.getUsername());
        assertEquals(personDto.getPassword(), person.getPassword());
        assertEquals(personDto.getPasswordConfirm(), person.getPasswordConfirm());
    }

    @Test
    public void toDto() {
        PersonDto personDto = PersonMapper.toDto(person);

        assertEquals(person.getId(), personDto.getId());
        assertEquals(person.getSurName(), personDto.getSurName());
        assertEquals(person.getFirstName(), personDto.getFirstName());
        assertEquals(person.getPhone(), personDto.getPhone());
        assertEquals(person.getEmail(), personDto.getEmail());
        assertEquals(person.getUsername(), personDto.getUsername());
    }
}