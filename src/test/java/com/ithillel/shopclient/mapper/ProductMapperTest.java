package com.ithillel.shopclient.mapper;

import com.ithillel.shopclient.dto.ProductDto;
import com.ithillel.shopclient.dto.ShopDto;

import com.ithillel.shopclient.model.Product;
import com.ithillel.shopclient.model.Shop;
import com.ithillel.shopclient.utils.mapper.ProductMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.ithillel.shopclient.ModelsFactory.createProduct;
import static com.ithillel.shopclient.ModelsFactory.createProductDto;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductMapperTest {
    private Product product;
    private ProductDto productDto;

    @Before
    public void setUp() {
        Shop shop = createShop();
        ShopDto shopDto = createShopDto();
        this.product = createProduct(shop);
        this.productDto = createProductDto(shopDto);
    }

    @Test
    public void fromDto() {
        Product product = ProductMapper.fromDto(productDto);

        assertEquals(productDto.getId(), product.getId());
        assertEquals(productDto.getName(), product.getName());
        assertEquals(productDto.getDescription(), product.getDescription());
        assertEquals(productDto.getShopName(), product.getShopId().getName());
        assertEquals(productDto.getPrice(), product.getPrice());
    }

    @Test
    public void toDto() {
        ProductDto productDto = ProductMapper.toDto(product);

        assertEquals(product.getId(), productDto.getId());
        assertEquals(product.getName(), productDto.getName());
        assertEquals(product.getDescription(), productDto.getDescription());
        assertEquals(product.getShopId().getName(), productDto.getShopName());
        assertEquals(product.getPrice(), productDto.getPrice());
    }
}