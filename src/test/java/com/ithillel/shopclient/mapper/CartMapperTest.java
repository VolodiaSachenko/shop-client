package com.ithillel.shopclient.mapper;

import com.ithillel.shopclient.dto.CartDto;
import com.ithillel.shopclient.dto.PersonDto;
import com.ithillel.shopclient.dto.ShopDto;
import com.ithillel.shopclient.model.Cart;
import com.ithillel.shopclient.model.Person;
import com.ithillel.shopclient.model.Shop;

import com.ithillel.shopclient.utils.mapper.CartMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.ithillel.shopclient.ModelsFactory.createCart;
import static com.ithillel.shopclient.ModelsFactory.createCartDto;
import static com.ithillel.shopclient.ModelsFactory.createPerson;
import static com.ithillel.shopclient.ModelsFactory.createPersonDto;
import static com.ithillel.shopclient.ModelsFactory.createShop;
import static com.ithillel.shopclient.ModelsFactory.createShopDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CartMapperTest {
    private Cart cart;
    private CartDto cartDto;

    @Before
    public void setUp() {
        PersonDto personDto = createPersonDto();
        ShopDto shopDto = createShopDto();
        this.cartDto = createCartDto(shopDto, personDto);

        Person person = createPerson();
        Shop shop = createShop();
        this.cart = createCart(shop, person);
    }


    @Test
    public void fromDto() {
        Cart cart = CartMapper.fromDto(cartDto);

        assertEquals(cartDto.getId(), cart.getId());
        assertEquals(cartDto.getSum(), cart.getSum());
        assertEquals(cartDto.getName(), cart.getName());

        assertNotNull(cart.getProducts());
        assertNotNull(cart.getShopId());
        assertNotNull(cart.getPersonId());
    }

    @Test
    public void toDto() {
        CartDto cartDto = CartMapper.toDto(cart);

        assertEquals(cart.getId(), cartDto.getId());
        assertEquals(cart.getSum(), cartDto.getSum());
        assertEquals(cart.getName(), cartDto.getName());

        assertNotNull(cartDto.getProducts());
        assertNotNull(cartDto.getShop());
        assertNotNull(cartDto.getPerson());
    }
}