FROM openjdk:8-jdk-alpine
EXPOSE 8080:8080
COPY ./target/shop.war /usr/app/
WORKDIR /usr/app/
ENTRYPOINT ["java","-jar","shop.war"]